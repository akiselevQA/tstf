//Find More Tutorials On WebDriver at -> http://software-testing-tutorials-automation.blogspot.com
package solution.testSuite.ManageDictionary;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.openqa.selenium.WebDriver;
import org.testng.ITestResult;
import org.testng.SkipException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.relevantcodes.extentreports.LogStatus;

import eccentex.AppTester.driver.SeleniumWebDriver;
import eccentex.AppTester.helper.Common;
import eccentex.AppTester.helper.Helpers;
import eccentex.AppTester.utility.Read_XLS;
import eccentex.AppTester.utility.SuiteUtility;
import eccentex.TestSuite.Base.SuiteBase;

//SuiteLoginCaseOne Class Inherits From SuiteOpenSolutionBase Class.
//So, SuiteLoginCaseOne Class Is Child Class Of SuiteOpenSolutionBase Class And SuiteBase Class.

public class DICT03 extends SuiteManageDictionaryBase{
	Read_XLS filePath = null;
	String sheetName = null;
	String testCaseName = null;	
	String toRunColumnNameTestCase = null;
	String toRunColumnNameTestData = null;
	String testDataToRun[]=null;
	static boolean testCasePass=true;
	static int dataSet=-1;	
	static boolean testSkip=false;
	static boolean testFail=false;
	SoftAssert s_assert =null;
	private Helpers helpers;
	private String menuItemName;
	public static String stringCodeToCleanUp;
	public static String siteURL;
	public static String testBrowser;
	public static SeleniumWebDriver seleniumWebDriver;	
	
	@BeforeTest
	public void checkCaseToRun() throws IOException{
/* DO NOT EDIT */		//get current test case name
/* DO NOT EDIT */		testCaseName = this.getClass().getSimpleName();
/* DO NOT EDIT */		//Called init() function from SuiteBase class to Initialize .xls Files
/* DO NOT EDIT */		init();
/* DO NOT EDIT */	
/* DO NOT EDIT */		extentTest = extentReport.startTest(testCaseName);
/* DO NOT EDIT */		extentTest.log(LogStatus.INFO, testCaseName + " configuration started");
/* DO NOT EDIT */		Add_Log.info(testCaseName + " configuration started");
/* DO NOT EDIT */	
/* DO NOT EDIT */		//To set SuiteLogin.xls file's path In filePath Variable.
/* DO NOT EDIT */		filePath = TestCaseListExcel;	
/* DO NOT EDIT */		//sheetName to check CaseToRun flag against test case.
/* DO NOT EDIT */		sheetName = "TestCasesList";
/* DO NOT EDIT */		//Name of column In TestCasesList Excel sheet.
/* DO NOT EDIT */		toRunColumnNameTestCase = "CaseToRun";
/* DO NOT EDIT */		//Name of column In Test Case Data sheets.
/* DO NOT EDIT */		toRunColumnNameTestData = "DataToRun";
/* DO NOT EDIT */		//To retrieve DataToRun flags of all data set lines from related test data sheet.
/* DO NOT EDIT */		testDataToRun = runOrSkipCase(filePath, testCaseName, sheetName, toRunColumnNameTestCase, toRunColumnNameTestData);
}
	
	//Accepts 4 column's String data In every Iteration.
	@Test(dataProvider="SuiteGetTestData")
	public void suiteManageDictAddDict(String IncomingDataCol1,String IncomingDataCol2,String IncomingDataCol3,String IncomingDataCol4,String IncomingDataCol5,String IncomingDataCol6,String IncomingexpectedResult) throws Throwable{
		DataCol1 = IncomingDataCol1;
		DataCol2 = IncomingDataCol2;
		DataCol3 = IncomingDataCol3;
		dataSet++;
		
		//Created object of testng SoftAssert class.
		s_assert = new SoftAssert();
		
		//If found DataToRun = "N" for data set then execution will be skipped for that data set.
		//If found DataToRun = "Y" for data set then bellow given lines will be executed.
		skipTestIfDataToRunIsN();
		/* DO NOT EDIT */
		/* DO NOT EDIT */		seleniumWebDriver = new SeleniumWebDriver();
		/* DO NOT EDIT */		

		if (Param == null){
			//INITIATE DRIVERS AND RUN BROWSER
			Param = new Properties();
    		FileInputStream fip = new FileInputStream(System.getProperty("user.dir")+"//src//solution//input//property//Param.properties");
			Param.load(fip);
		}
		
		for (String testBrowser : testBrowsers) {
			//To Initialize browser.
			driver =  seleniumWebDriver.loadDriver(testBrowser);
			helpers = new Helpers(driver);
			driver.manage().window().maximize();
			
			for (String siteURL : siteURLtoRUN) {
				SuiteBase.Add_Log.info("Run test for " + siteURL);
				extentTest.log(LogStatus.INFO, "Run test for " + siteURL);
				helpers.getAppbase().loginAndOpenSolution(siteURL, Param.getProperty("userLogin"), Param.getProperty("userPassword"), menuItemName, Param, helpers, driver);
			
			    //PERFORM TEST
		        /* Test input data
				*DataCol1 -- dictionary name
				*DataCol2 -- dictionary item name
				*DataCol3 -- order number
				*ExpectedResult */
	       
	        	//OPEN MENU ITEM
	        	helpers.getLeftmenu().clickItemByName("Dictionaries");
	        	helpers.getIFrame().switchIntoFrame("443025603_IFrame");
	        	
	        	if (helpers.getExtjs().getGridRowByContent(DataCol1.toUpperCase()) != null){
	    			helpers.getExtjs().getGridRowByContent(DataCol1.toUpperCase()).click();
	        	}
	        	DataCol1 = DataCol1+"Item";
	        	applyDictionaryItemFilter();
			}
        }
	}

	private void applyDictionaryItemFilter() {
		helpers.getExtjs().getGridRowByContent(DataCol1.toUpperCase()).click();
		helpers.getExtjs().getElementByID("textfield-1049-inputEl").clear(); //code filter
		helpers.getExtjs().getElementByID("textfield-1049-inputEl").sendKeys(DataCol1.toUpperCase());
		helpers.getExtjs().getElementByID("textfield-1050-inputEl").clear(); //code filter
		helpers.getExtjs().getElementByID("textfield-1050-inputEl").sendKeys(DataCol1);
		helpers.getExtjs().findElementByXPath("a", "id", "ecx-button-1052").click(); //search
		Common.waitTime(3);
		if(helpers.getExtjs().getGridRowByContent(DataCol3.toUpperCase()) != null){
			SuiteBase.Add_Log.info ("Dictionary filter works correctly");
			SuiteBase.Add_Log.error ("Dictionary filter not working");
		}
		else{
			SuiteBase.Add_Log.info ("Dictionary filter not working");
			extentTest.log(LogStatus.ERROR, "Dictionary filter not working");
			driver.close();		
		}
		
		helpers.getExtjs().findElementByXPath("a", "id", "ecx-button-1054").click(); //clear
		Common.waitTime(3);
		if(helpers.getExtjs().getGridRowByContent(DataCol3.toUpperCase()) != null){
			SuiteBase.Add_Log.info ("Dictionary filter works correctly");
			SuiteBase.Add_Log.error ("Dictionary filter not working");
		}
		else{
			SuiteBase.Add_Log.info ("Dictionary filter not working");
			extentTest.log(LogStatus.ERROR, "Dictionary filter not working");
			driver.close();		
		}
	}

	public void skipTestIfDataToRunIsN() {
		if(!testDataToRun[dataSet].equalsIgnoreCase("Y")){	
			Add_Log.info(testCaseName+" : DataToRun = N for data set line "+(dataSet+1)+" So skipping Its execution.");
			extentTest.log(LogStatus.INFO, testCaseName+" : DataToRun = N for data set line "+(dataSet+1)+" So skipping Its execution.");
			//If DataToRun = "N", Set testSkip=true.
			testSkip=true;
			throw new SkipException("DataToRun for row number "+dataSet+" Is No Or Blank. So Skipping Its Execution.");
		}
	}
	
	/* DO NOT EDIT */	//@AfterMethod method will be executed after execution of @Test method every time.
	/* DO NOT EDIT */	@AfterMethod
	/* DO NOT EDIT */	public void reporterDataResults(ITestResult result){		
	/* DO NOT EDIT */		afterMethodCheckStatus(testSkip, filePath, testCaseName, dataSet, testCasePass, result);
	/* DO NOT EDIT */		//At last make flag as false for next data set.
	/* DO NOT EDIT */		testSkip=false;
	/* DO NOT EDIT */	}
	
	//This data provider method will return 4 column's data one by one In every Iteration.
	@DataProvider
	public Object[][] SuiteGetTestData(){
		//To retrieve data from Data 1 Column,Data 2 Column,Data 3 Column and Expected Result column of SuiteLoginCaseOne data Sheet.
		//Last two columns (DataToRun and Pass/Fail/Skip) are Ignored programatically when reading test data.
		return SuiteUtility.GetTestDataUtility(filePath, testCaseName);
	}	
	
	//To report result as pass or fail for test cases In TestCasesList sheet.
	@AfterTest
	public void closeBrowser(){
		/* DO NOT EDIT */		Add_Log.info("TestCaseName = " + testCaseName + " , testCasePass = " + testCasePass + " , filePath =" + filePath + " , sheetName =" + sheetName);
		/* DO NOT EDIT */		extentTest.log(LogStatus.INFO, "TestCaseName = " + testCaseName + " , testCasePass = " + testCasePass + " , filePath =" + filePath + " , sheetName =" + sheetName);	
		/*
		SuiteBase.Add_Log.info("cleanUpTestRow method execution begins");
		if (siteURLSQL!=null){
			try {
				helpers.getJDBCMethod().deleteTestRowOracle(oracleDriver, oracleConnection, oracleUser, oraclePassword, Param, "tbl_dictionary", "col_code", DataCol1);
			} catch (SQLException e) {
				System.out.println(" INFO [error] deleteTestRowOracle failed");
				e.printStackTrace();
			}
		}
		if (siteURLORACLE!=null){
			helpers.getJDBCMethod().deleteTestRowSQL(mssqlDriver, mssqlName, mssqlUser, mssqlPassword, Param, "tbl_disctionary", "col_code", DataCol1);
		}
		*/
		/* DO NOT EDIT */		seleniumWebDriver.closeWebDriver(testCaseName, testCasePass, filePath, sheetName);
		/* DO NOT EDIT */		extentReport.endTest(extentTest);						
		/* DO NOT EDIT */		extentReport.flush();
	}
}

