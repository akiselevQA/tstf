/* DO NOT EDIT THIS PART */
/* DO NOT EDIT *///Find More Tutorials On WebDriver at -> http://software-testing-tutorials-automation.blogspot.com
/* DO NOT EDIT */package solution.testSuite.ManageDictionary;
/* DO NOT EDIT */import java.io.FileInputStream;
/* DO NOT EDIT */
/* DO NOT EDIT */import java.io.IOException;
/* DO NOT EDIT */import java.util.Properties;
/* DO NOT EDIT */
/* DO NOT EDIT */import org.openqa.selenium.WebDriver;
import org.testng.ITestResult;
/* DO NOT EDIT */import org.testng.SkipException;
/* DO NOT EDIT */import org.testng.annotations.AfterMethod;
/* DO NOT EDIT */import org.testng.annotations.AfterTest;
/* DO NOT EDIT */import org.testng.annotations.BeforeTest;
/* DO NOT EDIT */import org.testng.annotations.DataProvider;
/* DO NOT EDIT */import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.relevantcodes.extentreports.LogStatus;

import eccentex.AppTester.driver.SeleniumWebDriver;
/* DO NOT EDIT */import eccentex.AppTester.helper.Common;
/* DO NOT EDIT */import eccentex.AppTester.helper.Helpers;
/* DO NOT EDIT */import eccentex.AppTester.utility.Read_XLS;
/* DO NOT EDIT */import eccentex.AppTester.utility.SuiteUtility;
/* DO NOT EDIT */import eccentex.TestSuite.Base.SuiteBase;
/* DO NOT EDIT */
/* DO NOT EDIT *///SuiteLoginCaseOne Class Inherits From SuiteOpenSolutionBase Class.
/* DO NOT EDIT *///So, SuiteLoginCaseOne Class Is Child Class Of SuiteOpenSolutionBase Class And SuiteBase Class.
/* DO NOT EDIT */
/* DO NOT EDIT */public class DICT06 extends SuiteManageDictionaryBase{
/* DO NOT EDIT */	
/* DO NOT EDIT */	Read_XLS filePath = null;
/* DO NOT EDIT */	String sheetName = null;
/* DO NOT EDIT */	String testCaseName = null;	
/* DO NOT EDIT */	String toRunColumnNameTestCase = null;
/* DO NOT EDIT */	String toRunColumnNameTestData = null;
/* DO NOT EDIT */	String testDataToRun[]=null;
/* DO NOT EDIT */	static boolean testCasePass=true;
/* DO NOT EDIT */	static int dataSet=-1;	
/* DO NOT EDIT */	static boolean testSkip=false;
/* DO NOT EDIT */	static boolean testFail=false;
/* DO NOT EDIT */	private Helpers helpers;
/* DO NOT EDIT */	SoftAssert s_assert =null;
/* DO NOT EDIT */	public static String siteURL;
/* DO NOT EDIT */	public static String testBrowser;
/* DO NOT EDIT */	public static SeleniumWebDriver seleniumWebDriver;
/* DO NOT EDIT */	
/* DO NOT EDIT */	@BeforeTest
/* DO NOT EDIT */	public void checkCaseToRun() throws IOException{
/* DO NOT EDIT */		//get current test case name
/* DO NOT EDIT */		testCaseName = this.getClass().getSimpleName();
/* DO NOT EDIT */		//Called init() function from SuiteBase class to Initialize .xls Files
/* DO NOT EDIT */		init();
/* DO NOT EDIT */	
/* DO NOT EDIT */		extentTest = extentReport.startTest(testCaseName);
/* DO NOT EDIT */		extentTest.log(LogStatus.INFO, testCaseName + " configuration started");
/* DO NOT EDIT */		Add_Log.info(testCaseName + " configuration started");
/* DO NOT EDIT */	
/* DO NOT EDIT */		//To set SuiteLogin.xls file's path In filePath Variable.
/* DO NOT EDIT */		filePath = TestCaseListExcel;	
/* DO NOT EDIT */		//sheetName to check CaseToRun flag against test case.
/* DO NOT EDIT */		sheetName = "TestCasesList";
/* DO NOT EDIT */		//Name of column In TestCasesList Excel sheet.
/* DO NOT EDIT */		toRunColumnNameTestCase = "CaseToRun";
/* DO NOT EDIT */		//Name of column In Test Case Data sheets.
/* DO NOT EDIT */		toRunColumnNameTestData = "DataToRun";
/* DO NOT EDIT */		//To retrieve DataToRun flags of all data set lines from related test data sheet.
/* DO NOT EDIT */		testDataToRun = runOrSkipCase(filePath, testCaseName, sheetName, toRunColumnNameTestCase, toRunColumnNameTestData);
/* DO NOT EDIT */	}
/* DO NOT EDIT */	
/* DO NOT EDIT */	//Accepts 4 column's String data In every Iteration.
/* DO NOT EDIT */	@Test(dataProvider="SuiteGetTestData")
/* DO NOT EDIT */	public void suiteManageDictAddDict(String IncomingDataCol1,String IncomingDataCol2,String IncomingDataCol3,String IncomingDataCol4,String IncomingDataCol5,String IncomingDataCol6,String IncomingExpectedResult) throws Throwable{
/* DO NOT EDIT */		DataCol1 = IncomingDataCol1;
/* DO NOT EDIT */		DataCol2 = IncomingDataCol2;
/* DO NOT EDIT */		DataCol3 = IncomingDataCol3;
/* DO NOT EDIT */		dataSet++;

/* DO NOT EDIT */		//Created object of testng SoftAssert class.
/* DO NOT EDIT */		s_assert = new SoftAssert();
/* DO NOT EDIT */		
/* DO NOT EDIT */		//If found DataToRun = "N" for data set then execution will be skipped for that data set.
/* DO NOT EDIT */		//If found DataToRun = "Y" for data set then bellow given lines will be executed.
/* DO NOT EDIT */		skipTestIfDataToRunIsN();
/* DO NOT EDIT */		
/* DO NOT EDIT */
/* DO NOT EDIT */		seleniumWebDriver = new SeleniumWebDriver();
/* DO NOT EDIT */		
						if (Param == null){
/* DO NOT EDIT */			//INITIATE DRIVERS AND RUN BROWSER
/* DO NOT EDIT */			Param = new Properties();
/* DO NOT EDIT */    		FileInputStream fip = new FileInputStream(System.getProperty("user.dir")+"//src//solution//input//property//Param.properties");
/* DO NOT EDIT */			Param.load(fip);
						}
/* DO NOT EDIT */		
/* DO NOT EDIT */		for (String testBrowser : testBrowsers) {
/* DO NOT EDIT */			//To Initialize browser.
/* DO NOT EDIT */			driver =  seleniumWebDriver.loadDriver(testBrowser);
/* DO NOT EDIT */			helpers = new Helpers(driver);
/* DO NOT EDIT */			driver.manage().window().maximize();
/* DO NOT EDIT */		
/* DO NOT EDIT */        	for (String siteURL : siteURLtoRUN) {
/* DO NOT EDIT */        		SuiteBase.Add_Log.info("Run test for " + siteURL);
/* DO NOT EDIT */				extentTest.log(LogStatus.INFO, "Run test for " + siteURL);
/* DO NOT EDIT */        		helpers.getAppbase().loginAndOpenSolution(siteURL, Param.getProperty("userLogin"), Param.getProperty("userPassword"), menuItemName, Param, helpers, driver);
/* DO NOT EDIT */        
        		//OPEN MENU ITEM
        		helpers.getLeftmenu().clickItemByName("Dictionaries");
        		helpers.getIFrame().switchIntoFrame("443025603_IFrame");
        
        		//PERFORM TEST
    			/* Test input data
    			*DataCol1 -- dictionary name
    			*DataCol2 -- dictionary item name
    			*DataCol3 -- order number
    			*ExpectedResult */
        		DataCol2 = DataCol2+"Item";
        		createNewDictionaryItem();
        		validateNewDictionaryItem();
        	}
        }
	}

	private void createNewDictionaryItem() {
		SuiteBase.Add_Log.info("createNewDictionaryItem method execution begins");
		extentTest.log(LogStatus.INFO, "createNewDictionaryItem method execution begins");
		Common.waitTime(5);
		if (helpers.getExtjs().getGridRowByContent(DataCol1.toUpperCase()) != null){
			helpers.getExtjs().getGridRowByContent(DataCol1.toUpperCase()).click();
		
			//ADD DICTIONARY ATTRIBUTES
			if (helpers.getExtjs().getGridRowByContent(DataCol2.toUpperCase()) == null){
				helpers.getExtjs().findElementByXPath("span", "id", "ecx-new-button-1069-btnIconEl").click();
				populateDictionaryCategoryItemValues(DataCol2,DataCol3);
			}
			else{
				SuiteBase.Add_Log.info("Dictionary Item with name " + DataCol1 + " is already exists");
				extentTest.log(LogStatus.INFO, "Dictionary Item with name " + DataCol1 + " is already exists");
			}
		}
	}
	
	public void validateNewDictionaryItem() {
		helpers.getExtjs().getGridRowByContent(DataCol2.toUpperCase()).click();
		Common.waitTime(3);
		DataCol1 = DataCol1 + "Item";
		helpers.getExtjs().findElementByXPath("span", "id", "ecx-new-button-1069-btnIconEl").click(); //Add Category
		
		//Add Attributes
		populateDictionaryCategoryItemValues("","");
		Common.waitTime(4);
		helpers.getExtjs().validateMessageExists("Please check the form for any errors.");
		helpers.getExtjs().findOKAlertButton().click();
		helpers.getExtjs().getElementByID("ecx-cancel-button-1086-btnInnerEl").click();//Cancel
	
		//Add Attributes
		String symbols = "~!@#$%^&*()__+}{|:?/.-*I";
		if (helpers.getExtjs().getGridRowByContent(symbols) == null){ //validate that value not exists
			helpers.getExtjs().findElementByXPath("span", "id", "ecx-new-button-1069-btnIconEl").click(); //Add Category
			populateDictionaryCategoryItemValues(symbols,symbols);
			if (helpers.getExtjs().getGridRowByContent(symbols) == null){
				SuiteBase.Add_Log.error ("Dictionary with name " + symbols + " didn't added");
				extentTest.log(LogStatus.ERROR, "Dictionary with name " + symbols + " didn't added");
			}
		}
		else {
			SuiteBase.Add_Log.info ("Dictionary with name " + symbols + " is already exists");
			extentTest.log(LogStatus.INFO, "Dictionary with name " + symbols + " is already exists");
		}
			
		//Add Long Text
		String tooLongText = "IABCDEFGHIJKLMNOPQRSTUVWX"; //y is 26th
		if (helpers.getExtjs().getGridRowByContent(tooLongText) == null){ //validate that value not exists
			helpers.getExtjs().findElementByXPath("span", "id", "ecx-new-button-1069-btnIconEl").click(); //Add Category
			populateDictionaryCategoryItemValues(tooLongText+"Y",tooLongText+tooLongText+tooLongText+tooLongText+tooLongText+tooLongText);
			if (helpers.getExtjs().getGridRowByContent(tooLongText+"Y") != null){
				SuiteBase.Add_Log.error ("Dictionary with name " + symbols + "Y didn't added");
				extentTest.log(LogStatus.ERROR, "Dictionary with name " + symbols + "Y didn't added");
			}
			if (helpers.getExtjs().getGridRowByContent(tooLongText) != null){
				SuiteBase.Add_Log.info ("Dictionary with name " + symbols + " succesfully added");
				extentTest.log(LogStatus.INFO, "Dictionary with name " + symbols + " succesfully added");
			}
		}
		else {
			SuiteBase.Add_Log.info ("Dictionary with name " + symbols + " is already exists");
			extentTest.log(LogStatus.INFO, "Dictionary with name " + symbols + " is already exists");
		}
	}
	
	public void populateDictionaryCategoryItemValues(String DataCol2, String DataCol3) {
		helpers.getExtjs().getElementByID("textfield-1079-inputEl").sendKeys(DataCol2.toUpperCase());//Code
		helpers.getExtjs().getElementByID("textfield-1080-inputEl").sendKeys(DataCol2);//Name
		helpers.getExtjs().getElementByID("ecx-save-button-1084-btnInnerEl").click();//Save
	}
	
/* DO NOT EDIT THIS PART */
/* DO NOT EDIT */	public void skipTestIfDataToRunIsN() {
/* DO NOT EDIT */		if(!testDataToRun[dataSet].equalsIgnoreCase("Y")){	
/* DO NOT EDIT */			Add_Log.info(testCaseName+" : DataToRun = N for data set line "+(dataSet+1)+" So skipping Its execution.");
/* DO NOT EDIT */			extentTest.log(LogStatus.INFO, testCaseName+" : DataToRun = N for data set line "+(dataSet+1)+" So skipping Its execution.");
/* DO NOT EDIT */			//If DataToRun = "N", Set testSkip=true.
/* DO NOT EDIT */			testSkip=true;
/* DO NOT EDIT */			throw new SkipException("DataToRun for row number "+dataSet+" Is No Or Blank. So Skipping Its Execution.");
/* DO NOT EDIT */		}
/* DO NOT EDIT */	}
/* DO NOT EDIT */	
/* DO NOT EDIT */	//@AfterMethod method will be executed after execution of @Test method every time.
/* DO NOT EDIT */	@AfterMethod
/* DO NOT EDIT */	public void reporterDataResults(ITestResult result){		
/* DO NOT EDIT */		afterMethodCheckStatus(testSkip, filePath, testCaseName, dataSet, testCasePass, result);
/* DO NOT EDIT */		//At last make flag as false for next data set.
/* DO NOT EDIT */		testSkip=false;
/* DO NOT EDIT */	}
/* DO NOT EDIT */	
/* DO NOT EDIT */	//This data provider method will return 4 column's data one by one In every Iteration.
/* DO NOT EDIT */	@DataProvider
/* DO NOT EDIT */	public Object[][] SuiteGetTestData(){
/* DO NOT EDIT */		//To retrieve data from Data 1 Column,Data 2 Column,Data 3 Column and Expected Result column of SuiteLoginCaseOne data Sheet.
/* DO NOT EDIT */		//Last two columns (DataToRun and Pass/Fail/Skip) are Ignored programatically when reading test data.
/* DO NOT EDIT */		return SuiteUtility.GetTestDataUtility(filePath, testCaseName);
/* DO NOT EDIT */	}	
/* DO NOT EDIT */	
/* DO NOT EDIT */	//To report result as pass or fail for test cases In TestCasesList sheet.
/* DO NOT EDIT */	@AfterTest
/* DO NOT EDIT */	public void closeBrowser(){
/* DO NOT EDIT */		Add_Log.info("TestCaseName = " + testCaseName + " , testCasePass = " + testCasePass + " , filePath =" + filePath + " , sheetName =" + sheetName);
/* DO NOT EDIT */		extentTest.log(LogStatus.INFO, "TestCaseName = " + testCaseName + " , testCasePass = " + testCasePass + " , filePath =" + filePath + " , sheetName =" + sheetName);	
/* DO NOT EDIT */		
/* DO NOT EDIT */		//SuiteBase.Add_Log.info("cleanUpTestRow method execution begins");
/* DO NOT EDIT */		//if (siteURLSQL!=null){
/* DO NOT EDIT */		//	try {
/* DO NOT EDIT */		//		helpers.getJDBCMethod().deleteTestRowOracle(oracleDriver, oracleConnection, oracleUser, oraclePassword, Param, "tbl_dictionary", "col_code", DataCol1);
/* DO NOT EDIT */		//	} catch (SQLException e) {
/* DO NOT EDIT */		//		System.out.println(" INFO [error] deleteTestRowOracle failed");
/* DO NOT EDIT */		//		e.printStackTrace();
/* DO NOT EDIT */		//	}
/* DO NOT EDIT */		//}
/* DO NOT EDIT */		//if (siteURLORACLE!=null){
/* DO NOT EDIT */		//	helpers.getJDBCMethod().deleteTestRowSQL(mssqlDriver, mssqlName, mssqlUser, mssqlPassword, Param, "tbl_disctionary", "col_code", DataCol1);
/* DO NOT EDIT */		//}
/* DO NOT EDIT */		
/* DO NOT EDIT */		seleniumWebDriver.closeWebDriver(testCaseName, testCasePass, filePath, sheetName);
/* DO NOT EDIT */		extentReport.endTest(extentTest);						
/* DO NOT EDIT */		extentReport.flush();
/* DO NOT EDIT */	}
/* DO NOT EDIT */}