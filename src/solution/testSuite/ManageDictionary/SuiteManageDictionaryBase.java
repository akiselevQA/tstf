/* DO NOT EDIT THIS PART */	
/* DO NOT EDIT */package solution.testSuite.ManageDictionary;
/* DO NOT EDIT */
/* DO NOT EDIT */import java.io.IOException;
/* DO NOT EDIT */import java.util.ArrayList;
/* DO NOT EDIT */import java.util.Properties;
/* DO NOT EDIT */
/* DO NOT EDIT */import org.testng.annotations.BeforeSuite;
/* DO NOT EDIT */
/* DO NOT EDIT */import eccentex.AppTester.utility.Read_XLS;
/* DO NOT EDIT */import eccentex.TestSuite.Base.SuiteBase;
/* DO NOT EDIT */
/* DO NOT EDIT */public class SuiteManageDictionaryBase extends SuiteBase{

	//Initializing Test Suite (<test suite file name>.xls) File Path Using Constructor Of Read_XLS Utility Class.
	public static Read_XLS TestCaseListExcel = new Read_XLS(System.getProperty("user.dir")+"\\src\\solution\\input\\ExcelFiles\\SuiteManageDictionary.xls");
	
	//Set Site URL which to test SQL in this test suite
	//Server example: http://soldevqa13.eccentex.com:4000/Ecx.Web
	//appid example:  root_Dictionaries
	//environmentExample: DICT_ExtJS5.088
	public static String SiteURLSQL = "http://soldevqa13.eccentex.com:4000/Ecx.Web?appid=root_Dictionaries&d=DICT_ExtJS5.088";
	//Set Site URL which to test ORACLE in this test suite
	public static String SiteURLORACLE = null;
	//set test browser for current suite. Set null to ignore this browser.
	public static String TestBrowserMozilla = null;//"Mozilla";
	public static String TestBrowserChrome = "Chrome";
	public static String TestBrowserIE = null; //"IE";

	//menu item name which always exists in this solution
	public static String menuItemName = "Dictionaries";
	
	//Oracle DB parameters
	public static String OracleDriver = "oracle.jdbc.driver.oracledriver";
	public static String OracleConnection = "jdbc:oracle:thin:@localhost:1521:mkyong";
	public static String OracleUser = "user";
	public static String OraclePassword = "password";
		
	//MsSql DB parameters
	public static String MssqlDriver = "jdbc:sqlserver://laxsql";
	public static String MssqlName = "user_DICT_ExtJS5088";
	public static String MssqlUser = "user_DICT_ExtJS5088";
	public static String MssqlPassword = "p99d1fa9efaa441";
	
	/* DO NOT EDIT */	public static ArrayList<String> siteURLtoRUN = new ArrayList<String>();
	/* DO NOT EDIT */	public static ArrayList<String> testBrowsers = new ArrayList<String>();

	public static String DataCol1;
	public static String DataCol2;
	public static String DataCol3;
	public static String DataCol4;
	public static String DataCol5;
	public static String DataCol6;
	public static Properties Param;

/* DO NOT EDIT THIS PART */	
/* DO NOT EDIT */	//This function will be executed before SuiteLogin's test cases to check SuiteToRun flag.
/* DO NOT EDIT */	@BeforeSuite
/* DO NOT EDIT */	public void checkSuiteToRun() throws IOException{		
/* DO NOT EDIT */		//Called init() function from SuiteBase class to Initialize .xls Files
/* DO NOT EDIT */		init();


/* DO NOT EDIT */		//get site URL link
/* DO NOT EDIT */		//if !Param.getProperty("parameterSiteURL") {siteURL = Param.getProperty("parameterSiteURL"} else
/* DO NOT EDIT */		if (SiteURLSQL != null){
/* DO NOT EDIT */			siteURLtoRUN.add(SiteURLSQL);
/* DO NOT EDIT */		}
/* DO NOT EDIT */		if (SiteURLORACLE != null){
/* DO NOT EDIT */			siteURLtoRUN.add(SiteURLORACLE);
/* DO NOT EDIT */		}
/* DO NOT EDIT */		if (siteURLtoRUN.isEmpty()){Add_Log.error(this.getClass().getSimpleName()+" : no URL to check.");}
/* DO NOT EDIT */		
/* DO NOT EDIT */		//get test browser
/* DO NOT EDIT */		//if (!Param.getProperty("parameterTestBrowser")){testBrowser = Param.getProperty("parameterTestBrowser");} else
/* DO NOT EDIT */		if (TestBrowserMozilla != null){
/* DO NOT EDIT */			testBrowsers.add(TestBrowserMozilla);
/* DO NOT EDIT */		}
/* DO NOT EDIT */		if (TestBrowserChrome != null){
/* DO NOT EDIT */			testBrowsers.add(TestBrowserChrome);
/* DO NOT EDIT */		}
/* DO NOT EDIT */		if (TestBrowserIE != null){
/* DO NOT EDIT */			testBrowsers.add(TestBrowserIE);
/* DO NOT EDIT */		}
/* DO NOT EDIT */		if (testBrowsers.isEmpty()){Add_Log.error(this.getClass().getSimpleName()+" : no browsers to check.");}
/* DO NOT EDIT */		//To set TestSuiteList.xls file's path In filePath Variable.
/* DO NOT EDIT */		Read_XLS filePath = testSuiteListExcel;
/* DO NOT EDIT */		//To set all test in the suite
/* DO NOT EDIT */		String sheetName = "SuitesList";
/* DO NOT EDIT */		//to get the data from the tab
/* DO NOT EDIT */		String suiteName = "SuiteManageDictionary";
/* DO NOT EDIT */		//Column name SuiteToRun to get or set the status
/* DO NOT EDIT */		String toRunColumnName = "SuiteToRun";
/* DO NOT EDIT */		runOrSkipSuite(filePath, sheetName, suiteName, toRunColumnName);		
/* DO NOT EDIT */	}		
}



