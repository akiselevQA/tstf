/* DO NOT EDIT THIS PART */
/* DO NOT EDIT *///Find More Tutorials On WebDriver at -> http://software-testing-tutorials-automation.blogspot.com
/* DO NOT EDIT */package solution.testSuite.ManageDictionary;
/* DO NOT EDIT */import java.io.FileInputStream;
/* DO NOT EDIT */
/* DO NOT EDIT */import java.io.IOException;
/* DO NOT EDIT */import java.util.List;
/* DO NOT EDIT */import java.util.Properties;
/* DO NOT EDIT */import org.openqa.selenium.By;
/* DO NOT EDIT */
/* DO NOT EDIT */import org.openqa.selenium.WebDriver;
/* DO NOT EDIT */import org.openqa.selenium.WebElement;
/* DO NOT EDIT */import org.openqa.selenium.support.ui.ExpectedConditions;
/* DO NOT EDIT */import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.ITestResult;
/* DO NOT EDIT */import org.testng.SkipException;
/* DO NOT EDIT */import org.testng.annotations.AfterMethod;
/* DO NOT EDIT */import org.testng.annotations.AfterTest;
/* DO NOT EDIT */import org.testng.annotations.BeforeTest;
/* DO NOT EDIT */import org.testng.annotations.DataProvider;
/* DO NOT EDIT */import org.testng.annotations.Test;
/* DO NOT EDIT */import org.testng.asserts.SoftAssert;

import com.relevantcodes.extentreports.LogStatus;

import eccentex.AppTester.driver.SeleniumWebDriver;
/* DO NOT EDIT */import eccentex.AppTester.helper.Common;
/* DO NOT EDIT */import eccentex.AppTester.helper.Helpers;
/* DO NOT EDIT */import eccentex.AppTester.utility.Read_XLS;
/* DO NOT EDIT */import eccentex.AppTester.utility.SuiteUtility;
/* DO NOT EDIT */import eccentex.TestSuite.Base.SuiteBase;
/* DO NOT EDIT */
/* DO NOT EDIT *///SuiteLoginCaseOne Class Inherits From SuiteOpenSolutionBase Class.
/* DO NOT EDIT *///So, SuiteLoginCaseOne Class Is Child Class Of SuiteOpenSolutionBase Class And SuiteBase Class.
/* DO NOT EDIT */
/* DO NOT EDIT */public class DICT08 extends SuiteManageDictionaryBase{
/* DO NOT EDIT */	
/* DO NOT EDIT */	Read_XLS filePath = null;
/* DO NOT EDIT */	String sheetName = null;
/* DO NOT EDIT */	String testCaseName = null;	
/* DO NOT EDIT */	String toRunColumnNameTestCase = null;
/* DO NOT EDIT */	String toRunColumnNameTestData = null;
/* DO NOT EDIT */	String testDataToRun[]=null;
/* DO NOT EDIT */	static boolean testCasePass=true;
/* DO NOT EDIT */	static int dataSet=-1;	
/* DO NOT EDIT */	static boolean testSkip=false;
/* DO NOT EDIT */	static boolean testFail=false;
/* DO NOT EDIT */	SoftAssert s_assert =null;
/* DO NOT EDIT */	private Helpers helpers;
/* DO NOT EDIT */	public static String siteURL;
/* DO NOT EDIT */	public static String testBrowser;
/* DO NOT EDIT */	public static String menuItemName;
/* DO NOT EDIT */	public static SeleniumWebDriver seleniumWebDriver;
/* DO NOT EDIT */	
/* DO NOT EDIT */	@BeforeTest
/* DO NOT EDIT */	public void checkCaseToRun() throws IOException{
/* DO NOT EDIT */		//get current test case name
/* DO NOT EDIT */		testCaseName = this.getClass().getSimpleName();
/* DO NOT EDIT */		//Called init() function from SuiteBase class to Initialize .xls Files
/* DO NOT EDIT */		init();
/* DO NOT EDIT */	
/* DO NOT EDIT */		extentTest = extentReport.startTest(testCaseName);
/* DO NOT EDIT */		extentTest.log(LogStatus.INFO, testCaseName + " configuration started");
/* DO NOT EDIT */		Add_Log.info(testCaseName + " configuration started");
/* DO NOT EDIT */	
/* DO NOT EDIT */		//To set SuiteLogin.xls file's path In filePath Variable.
/* DO NOT EDIT */		filePath = TestCaseListExcel;	
/* DO NOT EDIT */		//sheetName to check CaseToRun flag against test case.
/* DO NOT EDIT */		sheetName = "TestCasesList";
/* DO NOT EDIT */		//Name of column In TestCasesList Excel sheet.
/* DO NOT EDIT */		toRunColumnNameTestCase = "CaseToRun";
/* DO NOT EDIT */		//Name of column In Test Case Data sheets.
/* DO NOT EDIT */		toRunColumnNameTestData = "DataToRun";
/* DO NOT EDIT */		//To retrieve DataToRun flags of all data set lines from related test data sheet.
/* DO NOT EDIT */		testDataToRun = runOrSkipCase(filePath, testCaseName, sheetName, toRunColumnNameTestCase, toRunColumnNameTestData);
/* DO NOT EDIT */	}
/* DO NOT EDIT */	
/* DO NOT EDIT */	//Accepts 4 column's String data In every Iteration.
/* DO NOT EDIT */	@Test(dataProvider="SuiteGetTestData")
/* DO NOT EDIT */	public void suiteManageDictAddDict(String IncomingDataCol1,String IncomingDataCol2,String IncomingDataCol3,String IncomingDataCol4,String IncomingDataCol5,String IncomingDataCol6,String IncomingExpectedResult) throws Throwable{
/* DO NOT EDIT */		DataCol1 = IncomingDataCol1;
/* DO NOT EDIT */		DataCol2 = IncomingDataCol2;
/* DO NOT EDIT */		DataCol3 = IncomingDataCol3;
/* DO NOT EDIT */		dataSet++;
/* DO NOT EDIT */		
/* DO NOT EDIT */		//Created object of testng SoftAssert class.
/* DO NOT EDIT */		s_assert = new SoftAssert();
/* DO NOT EDIT */		
/* DO NOT EDIT */		//If found DataToRun = "N" for data set then execution will be skipped for that data set.
/* DO NOT EDIT */		//If found DataToRun = "Y" for data set then bellow given lines will be executed.
/* DO NOT EDIT */		skipTestIfDataToRunIsN();
/* DO NOT EDIT */
/* DO NOT EDIT */		seleniumWebDriver = new SeleniumWebDriver();
/* DO NOT EDIT */		
						if (Param == null){
/* DO NOT EDIT */			//INITIATE DRIVERS AND RUN BROWSER
/* DO NOT EDIT */			Param = new Properties();
/* DO NOT EDIT */    		FileInputStream fip = new FileInputStream(System.getProperty("user.dir")+"//src//solution//input//property//Param.properties");
/* DO NOT EDIT */			Param.load(fip);
						}
/* DO NOT EDIT */		
/* DO NOT EDIT */		for (String testBrowser : testBrowsers) {
/* DO NOT EDIT */			//To Initialize browser.
/* DO NOT EDIT */			driver =  seleniumWebDriver.loadDriver(testBrowser);
/* DO NOT EDIT */			helpers = new Helpers(driver);
/* DO NOT EDIT */			driver.manage().window().maximize();
/* DO NOT EDIT */		
/* DO NOT EDIT */        	for (String siteURL : siteURLtoRUN) {
/* DO NOT EDIT */        		SuiteBase.Add_Log.info("Run test for " + siteURL);
/* DO NOT EDIT */				extentTest.log(LogStatus.INFO, "Run test for " + siteURL);
/* DO NOT EDIT */        		helpers.getAppbase().loginAndOpenSolution(siteURL, Param.getProperty("userLogin"), Param.getProperty("userPassword"), menuItemName, Param, helpers, driver);
/* DO NOT EDIT */        
        		//OPEN MENU ITEM
        		helpers.getLeftmenu().clickItemByName("Dictionaries");
        		helpers.getIFrame().switchIntoFrame("443025603_IFrame");
        
        		//PERFORM TEST
    			/* Test input data
    			*DataCol1 -- dictionary name
    			*DataCol2 -- dictionary item name
    			*DataCol3 -- order number
    			*ExpectedResult */
        		modifyDictionaryCategoryItem();
        	}
        }
	}

	private void modifyDictionaryCategoryItem() {
		SuiteBase.Add_Log.info("modifyDictionaryCategoryItem method execution begins");
		extentTest.log(LogStatus.INFO,"modifyDictionaryCategoryItem method execution begins");
		Common.waitTime(3);
		helpers.getExtjs().getGridRowByContent(DataCol1.toUpperCase()).click();
        DataCol1 = DataCol1+"Item";
		helpers.getExtjs().getElementByID("textfield-1050-inputEl").clear(); // filter
		helpers.getExtjs().getElementByID("textfield-1050-inputEl").sendKeys(DataCol1);
		helpers.getExtjs().findElementByXPath("a", "id", "ecx-button-1052").click(); //search
		Common.waitTime(3);
       
		findRowTDHorizontal(DataCol1);
		Common.waitTime(2);
	       
		//helpers.getExtjs().findButtonByText("Quick Modify");

		helpers.getExtjs().findElementByXPath("div", "class", "x-menu-item-icon-default x-menu-item-icon fa-pencil fa-lg fa ").click();
		Common.waitTime(2);
		helpers.getExtjs().validateFieldIsNotEditable("textfield-1079-inputEl");
		helpers.getExtjs().getElementByID("textfield-1080-inputEl").sendKeys(" New");//Name
		helpers.getExtjs().getElementByID("ecx-save-button-1084-btnInnerEl").click();//Save
		
		//helpers.getExtjs().getElementByID("textfield-1049-inputEl").clear(); // filter
		//helpers.getExtjs().getElementByID("textfield-1049-inputEl").sendKeys(DataCol1.toUpperCase());
		//helpers.getExtjs().findElementByXPath("a", "id", "ecx-button-1017").click(); //search
		Common.waitTime(3);
		
		if (helpers.getExtjs().getGridRowByContent(DataCol1+" New") == null) {driver.close();}
		helpers.getExtjs().findElementByXPath("a", "id", "ecx-button-1052");

	}

	private void findRowTDHorizontal(String text) {
			(new WebDriverWait(driver, 3)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(".x-grid")));
			List<WebElement> gridsList = driver.findElements(By.cssSelector(".x-grid"));
			WebElement grid = gridsList.get(1);
			//WebElement grid = table.findElement(By.cssSelector(".x-grid"));
			List<WebElement> tds = grid.findElements(By.tagName("td"));
			//List<WebElement> tds = grid.findElements(By.cssSelector(".x-grid-cell-inner"));
			for (int i = 0; i < tds.size(); i++) {
			        WebElement cell = tds.get(i);
			        String curTest = cell.getText();
			        if (curTest.contains(text)) {
	
			  
	
			            List<WebElement> buttons = grid.findElements(By.cssSelector("span.x-btn-icon-el.x-btn-icon-el-default-small.fa-cog.fa"));
			            //List<WebElement> buttons = grid.findElements(By.cssSelector("span.x-btn-icon-el-default-small"));
			            //class="x-btn-icon-el x-btn-icon-el-default-small fa-cog fa "
			            buttons.get(0).click();
			            break;
			        }
			    }
			
			SuiteBase.Add_Log.info("Row actions menu opened");
			extentTest.log(LogStatus.INFO,"Row actions menu opened");
			
		    //model.findOptionByTextInButtonMenu("Disable").click();
		    //model.findOptionByTextInButtonMenu("Delete").click();	
	}
	public void populateDictionaryCategoryItemValues() {
		helpers.getExtjs().getElementByID("textfield-1079-inputEl").sendKeys(DataCol1.substring(0, 1).toUpperCase());//Code
		helpers.getExtjs().getElementByID("textfield-1080-inputEl").sendKeys(DataCol1.substring(0, 1));//Name
		helpers.getExtjs().getElementByID("textareafield-1082-inputEl").sendKeys(DataCol2+"1");//Description
		helpers.getExtjs().getElementByID("ecx-save-button-1084-btnInnerEl").click();//Save
	}
	
/* DO NOT EDIT THIS PART */
/* DO NOT EDIT */	public void skipTestIfDataToRunIsN() {
/* DO NOT EDIT */		if(!testDataToRun[dataSet].equalsIgnoreCase("Y")){	
/* DO NOT EDIT */			Add_Log.info(testCaseName+" : DataToRun = N for data set line "+(dataSet+1)+" So skipping Its execution.");
/* DO NOT EDIT */			extentTest.log(LogStatus.INFO, testCaseName+" : DataToRun = N for data set line "+(dataSet+1)+" So skipping Its execution.");
/* DO NOT EDIT */			//If DataToRun = "N", Set testSkip=true.
/* DO NOT EDIT */			testSkip=true;
/* DO NOT EDIT */			throw new SkipException("DataToRun for row number "+dataSet+" Is No Or Blank. So Skipping Its Execution.");
/* DO NOT EDIT */		}
/* DO NOT EDIT */	}
/* DO NOT EDIT */	
/* DO NOT EDIT */	//@AfterMethod method will be executed after execution of @Test method every time.
/* DO NOT EDIT */	@AfterMethod
/* DO NOT EDIT */	public void reporterDataResults(ITestResult result){		
/* DO NOT EDIT */		afterMethodCheckStatus(testSkip, filePath, testCaseName, dataSet, testCasePass, result);
/* DO NOT EDIT */		//At last make flag as false for next data set.
/* DO NOT EDIT */		testSkip=false;
/* DO NOT EDIT */	}
/* DO NOT EDIT */	
/* DO NOT EDIT */	//This data provider method will return 4 column's data one by one In every Iteration.
/* DO NOT EDIT */	@DataProvider
/* DO NOT EDIT */	public Object[][] SuiteGetTestData(){
/* DO NOT EDIT */		//To retrieve data from Data 1 Column,Data 2 Column,Data 3 Column and Expected Result column of SuiteLoginCaseOne data Sheet.
/* DO NOT EDIT */		//Last two columns (DataToRun and Pass/Fail/Skip) are Ignored programatically when reading test data.
/* DO NOT EDIT */		return SuiteUtility.GetTestDataUtility(filePath, testCaseName);
/* DO NOT EDIT */	}	
/* DO NOT EDIT */	
/* DO NOT EDIT */	//To report result as pass or fail for test cases In TestCasesList sheet.
/* DO NOT EDIT */	@AfterTest
/* DO NOT EDIT */	public void closeBrowser(){
/* DO NOT EDIT */		Add_Log.info("TestCaseName = " + testCaseName + " , testCasePass = " + testCasePass + " , filePath =" + filePath + " , sheetName =" + sheetName);
/* DO NOT EDIT */		extentTest.log(LogStatus.INFO, "TestCaseName = " + testCaseName + " , testCasePass = " + testCasePass + " , filePath =" + filePath + " , sheetName =" + sheetName);	
/* DO NOT EDIT */		
/* DO NOT EDIT */		//SuiteBase.Add_Log.info("cleanUpTestRow method execution begins");
/* DO NOT EDIT */		//if (siteURLSQL!=null){
/* DO NOT EDIT */		//	try {
/* DO NOT EDIT */		//		helpers.getJDBCMethod().deleteTestRowOracle(oracleDriver, oracleConnection, oracleUser, oraclePassword, Param, "tbl_dictionary", "col_code", DataCol1);
/* DO NOT EDIT */		//	} catch (SQLException e) {
/* DO NOT EDIT */		//		System.out.println(" INFO [error] deleteTestRowOracle failed");
/* DO NOT EDIT */		//		e.printStackTrace();
/* DO NOT EDIT */		//	}
/* DO NOT EDIT */		//}
/* DO NOT EDIT */		//if (siteURLORACLE!=null){
/* DO NOT EDIT */		//	helpers.getJDBCMethod().deleteTestRowSQL(mssqlDriver, mssqlName, mssqlUser, mssqlPassword, Param, "tbl_disctionary", "col_code", DataCol1);
/* DO NOT EDIT */		//}
/* DO NOT EDIT */		
/* DO NOT EDIT */		seleniumWebDriver.closeWebDriver(testCaseName, testCasePass, filePath, sheetName);
/* DO NOT EDIT */		extentReport.endTest(extentTest);						
/* DO NOT EDIT */		extentReport.flush();
/* DO NOT EDIT */	}
/* DO NOT EDIT */}