//Find More Tutorials On WebDriver at -> http://software-testing-tutorials-automation.blogspot.com
package solution.testSuite.OpenSolution;

import java.io.IOException;

import org.testng.ITestResult;
import org.testng.SkipException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.relevantcodes.extentreports.LogStatus;

import eccentex.AppTester.driver.SeleniumWebDriver;
import eccentex.AppTester.helper.Helpers;
import eccentex.AppTester.utility.Read_XLS;
import eccentex.AppTester.utility.SuiteUtility;

//SuiteLoginCaseOne Class Inherits From SuiteOpenSolutionBase Class.
//So, SuiteLoginCaseOne Class Is Child Class Of SuiteOpenSolutionBase Class And SuiteBase Class.

public class SuiteOpenSolution_OpenSolution extends SuiteOpenSolutionBase{
	Read_XLS filePath = null;
	String sheetName = null;
	String testCaseName = null;	
	String toRunColumnNameTestCase = null;
	String toRunColumnNameTestData = null;
	String testDataToRun[]=null;
	static boolean testCasePass=true;
	static int dataSet=-1;	
	static boolean testSkip=false;
	static boolean testFail=false;
	SoftAssert s_assert =null;
	private Helpers helpers;
	private SeleniumWebDriver seleniumWebDriver;
	private String menuItemName;
	public static String siteURL;
	public static String testBrowser;
	
	@BeforeTest
	public void checkCaseToRun() throws IOException{
		//Called init() function from SuiteBase class to Initialize .xls Files
		init();			
		//To set SuiteLogin.xls file's path In filePath Variable.
		testCaseName = this.getClass().getSimpleName();	
		//sheetName to check CaseToRun flag against test case.
		sheetName = "TestCasesList";
		//Name of column In TestCasesList Excel sheet.
		toRunColumnNameTestCase = "CaseToRun";
		//Name of column In Test Case Data sheets.
		toRunColumnNameTestData = "DataToRun";
		//To set SuiteLogin.xls file's path In filePath Variable.
		filePath = TestCaseListExcel;		
		//get site URL link
		//if !Param.getProperty("parameterSiteURL") {siteURL = Param.getProperty("parameterSiteURL"} else
		siteURL = SiteURL;
		//navigate to proper solution
		//menu item name which always exists in this solution
		menuItemName = MenuItemName;
		//get test browser
		//if (!Param.getProperty("parameterTestBrowser")){testBrowser = Param.getProperty("parameterTestBrowser");} else
		testBrowser = TestBrowser;
		//Bellow given syntax will Insert log In applog.log file.
		Add_Log.info(testCaseName+" : Execution started.");
		//To retrieve DataToRun flags of all data set lines from related test data sheet.
		testDataToRun = runOrSkipCase(filePath, testCaseName, sheetName, toRunColumnNameTestCase, toRunColumnNameTestData);
	}
	
	//Accepts 4 column's String data In every Iteration.
	@Test(dataProvider="SuiteGetTestData")
	public void suiteLoginCaseLoginTest(String DataCol1,String DataCol2,String DataCol3,String expectedResult) throws Throwable{
		
		dataSet++;
		
		init();
		
		testCaseName = this.getClass().getSimpleName();
		//Called init() function from SuiteBase class to Initialize .xls Files
		extentTest = extentReport.startTest(testCaseName);
		extentTest.log(LogStatus.INFO, testCaseName + " configuration started");
		
		//Created object of testng SoftAssert class.
		s_assert = new SoftAssert();
		
		//If found DataToRun = "N" for data set then execution will be skipped for that data set.
		//If found DataToRun = "Y" for data set then bellow given lines will be executed.
		skipTestIfDataToRunIsN();
		
		
		//init(); //MAY BE SHOULD RESTORE IT
		
		//INITIATE DRIVERS AND RUN BROWSER
		/*Param = new Properties();
    	FileInputStream fip = new FileInputStream(System.getProperty("user.dir")+"//src//solution//input//property//Param.properties");
		Param.load(fip);
		*/
		seleniumWebDriver = new SeleniumWebDriver();
		driver =  seleniumWebDriver.loadDriver(testBrowser);
		helpers = new Helpers(driver);
        
        //To Initialize browser.
        driver.manage().window().maximize();
                
        //PERFORM TEST
        /* Test input data
		*DataCol1 -- login
		*DataCol2 -- password
		*ExpectedResult */		
        helpers.getAppbase().loginAndOpenSolution(siteURL, DataCol1, DataCol2, menuItemName, Param, helpers, driver);
	}

	public void skipTestIfDataToRunIsN() {
		if(!testDataToRun[dataSet].equalsIgnoreCase("Y")){	
			Add_Log.info(testCaseName+" : DataToRun = N for data set line "+(dataSet+1)+" So skipping Its execution.");
			//If DataToRun = "N", Set testSkip=true.
			testSkip=true;
			throw new SkipException("DataToRun for row number "+dataSet+" Is No Or Blank. So Skipping Its Execution.");
		}
	}
	
	/* DO NOT EDIT */	//@AfterMethod method will be executed after execution of @Test method every time.
	/* DO NOT EDIT */	@AfterMethod
	/* DO NOT EDIT */	public void reporterDataResults(ITestResult result){		
	/* DO NOT EDIT */		afterMethodCheckStatus(testSkip, filePath, testCaseName, dataSet, testCasePass, result);
	/* DO NOT EDIT */		//At last make flag as false for next data set.
	/* DO NOT EDIT */		testSkip=false;
	/* DO NOT EDIT */	}
	
	//This data provider method will return 4 column's data one by one In every Iteration.
	@DataProvider
	public Object[][] SuiteGetTestData(){
		//To retrieve data from Data 1 Column,Data 2 Column,Data 3 Column and Expected Result column of SuiteLoginCaseOne data Sheet.
		//Last two columns (DataToRun and Pass/Fail/Skip) are Ignored programatically when reading test data.
		return SuiteUtility.GetTestDataUtility(filePath, testCaseName);
	}	
	
	//To report result as pass or fail for test cases In TestCasesList sheet.
	@AfterTest
	public void closeBrowser(){
		//To Close the web browser at the end of test.
		seleniumWebDriver.closeWebDriver(testCaseName, testCasePass, filePath, sheetName);
	}
}
