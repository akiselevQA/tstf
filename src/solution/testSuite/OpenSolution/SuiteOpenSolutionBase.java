package solution.testSuite.OpenSolution;

import java.io.IOException;

import org.testng.annotations.BeforeSuite;

import eccentex.AppTester.utility.Read_XLS;
import eccentex.TestSuite.Base.SuiteBase;

public class SuiteOpenSolutionBase extends SuiteBase{
	public static Read_XLS TestCaseListExcel = new Read_XLS(System.getProperty("user.dir")+"\\src\\solution\\input\\ExcelFiles\\SuiteOpenSolution.xls");
	//Set Site URL which to test in this test suite
	String SiteURL = "http://soldevqa13.eccentex.com:4000/Ecx.Web?appid=root_Dictionaries&d=DICT_ExtJS5.088";
	//set test browser for current suite
	public static String TestBrowser = "Chrome";
	//menu item name which always exists in this solution
	public static String MenuItemName = "Dictionaries";
	//environment name
	public static String EnvironmentNameArg = "DICT_ExtJS5";
	//solution name
	public static String SolutionNameArg = "DICT Dictionaries ExtJS5";
	//application name		
	public static String AppNameArg = "Dictionaries";
		
		
	//This function will be executed before SuiteLogin's test cases to check SuiteToRun flag.
	@BeforeSuite
	public void checkSuiteToRun() throws IOException{		
		//Called init() function from SuiteBase class to Initialize .xls Files
		init();			
		//To set TestSuiteList.xls file's path In filePath Variable.
		Read_XLS filePath = testSuiteListExcel;
		String sheetName = "SuitesList";
		String suiteName = "SuiteLogin";
		String toRunColumnName = "SuiteToRun";
		runOrSkipSuite(filePath, sheetName, suiteName, toRunColumnName);		
	}		
}