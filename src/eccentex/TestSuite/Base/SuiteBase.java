//Find More Tutorials On WebDriver at -> http://software-testing-tutorials-automation.blogspot.com
package eccentex.TestSuite.Base;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.testng.ITestResult;
import org.testng.SkipException;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import eccentex.AppTester.driver.SeleniumWebDriver;
import eccentex.AppTester.utility.ExtentReport;
import eccentex.AppTester.utility.Logging;
import eccentex.AppTester.utility.Read_XLS;
import eccentex.AppTester.utility.ScreenshotUtility;
import eccentex.AppTester.utility.SuiteUtility;

public class SuiteBase {
	
	public static Read_XLS testSuiteListExcel=null;
	public static Read_XLS testCaseListExcelOne=null;
	public static Read_XLS testCaseListExcelTwo=null;
	public static Read_XLS testCaseListExcelThree=null;
	public static Read_XLS testCaseListExcelFour=null;
	public static Logger Add_Log = null;
	public boolean browserAlreadyLoaded=false;
	public static Properties Param = null;
	public static Properties Object = null;
	public static WebDriver driver=null;
	public static WebDriver existingchromeBrowser;
	public static WebDriver existingmozillaBrowser;
	public static WebDriver existingIEBrowser;
	protected static ExtentReports extentReport;
	public static ExtentTest extentTest;


	public void init() throws IOException{
		//-----------To Initialize logger service.
		if (Add_Log == null){
			Add_Log = Logging.getLogger();
		}
		
		//-----------To Initialize http://extentreports.relevantcodes.com/ service
		if (extentReport == null){
			extentReport = ExtentReport.getExtentReport();
		}
		
		/*if ((logger == null) && (extentTest !=null)){
			logger = new Logging();
			Logging.setExtentTest(extentTest);
		}
		*/
		
		//Please change file's path strings bellow If you have stored them at location other than bellow.
		//Initializing Test Suite List(TestSuiteList.xls) File Path Using Constructor Of Read_XLS Utility Class.
		testSuiteListExcel = new Read_XLS(System.getProperty("user.dir")+"\\src\\solution\\input\\ExcelFiles\\TestSuiteList.xls");
		//Bellow given syntax will Insert log In applog.log file.
		Add_Log.info("All Excel Files Initialised successfully.");
		
		//Initialize Param.properties file.
		Param = new Properties();
		FileInputStream fip = new FileInputStream(System.getProperty("user.dir")+"//src//solution//input//property//Param.properties");
		Param.load(fip);
		Add_Log.info("Param.properties file loaded successfully.");		
	
		//Initialize Objects.properties file.
		Object = new Properties();
		fip = new FileInputStream(System.getProperty("user.dir")+"//src//solution//input//property//Objects.properties");
		Object.load(fip);
		Add_Log.info("Objects.properties file loaded successfully.");
	}
	
	public String[] runOrSkipCase(Read_XLS filePath, String testCaseName, String sheetName, String toRunColumnNameTestCase, String toRunColumnNameTestData) {
		//To check test case's CaseToRun = Y or N In related excel sheet.
		//If CaseToRun = N or blank, Test case will skip execution. Else It will be executed.
		if(!SuiteUtility.checkToRunUtility(filePath, sheetName,toRunColumnNameTestCase,testCaseName)){
			Add_Log.info(testCaseName+" : CaseToRun = N for So Skipping Execution.");
			extentTest.log(LogStatus.SKIP, testCaseName+" : CaseToRun = N for So Skipping Execution.");
			//To report result as skip for test cases In TestCasesList sheet.
			SuiteUtility.WriteResultUtility(filePath, sheetName, "Pass/Fail/Skip", testCaseName, "SKIP");
			//To throw skip exception for this test case.
			throw new SkipException(testCaseName+"'s CaseToRun Flag Is 'N' Or Blank. So Skipping Execution Of "+testCaseName);
		}	
		//To retrieve DataToRun flags of all data set lines from related test data sheet.
		return SuiteUtility.checkToRunUtilityOfData(filePath, testCaseName, toRunColumnNameTestData);
	}
	
	//check if we should run suite or skip it
	public void runOrSkipSuite(Read_XLS filePath, String sheetName, String suiteName, String toRunColumnName) {
		//Bellow given syntax will Insert log In applog.log file.
		Add_Log.info("Execution started for " + suiteName + ".");
		
		//If SuiteToRun !== "y" then SuiteLogin will be skipped from execution.
		if(!SuiteUtility.checkToRunUtility(filePath, sheetName,toRunColumnName,suiteName)){			
			Add_Log.info("SuiteToRun = N for "+suiteName+" So Skipping Execution.");
			//To report SuiteLogin as 'Skipped' In SuitesList sheet of TestSuiteList.xls If SuiteToRun = N.
			SuiteUtility.WriteResultUtility(filePath, sheetName, "Skipped/Executed", suiteName, "Skipped");
			//It will throw SkipException to skip test suite's execution and suite will be marked as skipped In testng report.
			throw new SkipException(suiteName+"'s SuiteToRun Flag Is 'N' Or Blank. So Skipping Execution Of "+suiteName);
		}
		//To report Suite as 'Executed' In SuitesList sheet of TestSuiteList.xls If SuiteToRun = Y.
		SuiteUtility.WriteResultUtility(filePath, sheetName, "Skipped/Executed", suiteName, "Executed");
	}
	
	public void afterMethodCheckStatus(boolean testskip, Read_XLS filePath, String testCaseName, int dataSet, boolean testCasePass, ITestResult result) {
		if(result.getStatus() == ITestResult.FAILURE)
	    {
	        String screenShotPath = ScreenshotUtility.captureScreenShot(result, null);
	        extentTest.log(LogStatus.FAIL, testCaseName+" : Reporting test data set line "+(dataSet+1)+" as FAIL In excel.");
	        extentTest.log(LogStatus.FAIL, result.getThrowable());
	        extentTest.log(LogStatus.FAIL, "Snapshot below: " + extentTest.addScreenCapture(screenShotPath));
	        Add_Log.error(testCaseName+" : Reporting test data set line "+(dataSet+1)+" as FAIL In excel.");
	        Add_Log.error(result.getThrowable());
			//Set TestCasePass = false to report test case as fail In excel sheet.
			testCasePass=false;	
			//If found testFail = true, Result will be reported as FAIL against data set line In excel sheet.
			SuiteUtility.WriteResultUtility(filePath, testCaseName, "Pass/Fail/Skip", dataSet+1, "FAIL");
	    }
		else if(testskip){
			Add_Log.info(testCaseName+" : Reporting test data set line "+(dataSet+1)+" as SKIP In excel.");
			//If found testSkip = true, Result will be reported as SKIP against data set line In excel sheet.
			extentTest.log(LogStatus.SKIP, testCaseName+" : Reporting test data set line "+(dataSet+1)+" as SKIP In excel.");
	        SuiteUtility.WriteResultUtility(filePath, testCaseName, "Pass/Fail/Skip", dataSet+1, "SKIP");
		}
		else{
			Add_Log.info(testCaseName+" : Reporting test data set line "+(dataSet+1)+" as PASS In excel.");
			//If found testSkip = false and testFail = false, Result will be reported as PASS against data set line In excel sheet.
			SuiteUtility.WriteResultUtility(filePath, testCaseName, "Pass/Fail/Skip", dataSet+1, "PASS");
		}
	}
}
