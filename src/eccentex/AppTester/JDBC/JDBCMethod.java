package eccentex.AppTester.JDBC;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

import com.relevantcodes.extentreports.LogStatus;

import eccentex.TestSuite.Base.SuiteBase;

public class JDBCMethod {
	private static Statement statement = null;
	private static Connection dbConnection = null;
	private static Properties Param;
	private static String DataCol1;
	private static String tableName;
	private JDBCMethod jdbcMethod;
	
	public JDBCMethod(JDBCMethod jdbcMethod) {
		this.jdbcMethod = jdbcMethod;
	}
	
	public JDBCMethod getJDBCMethod() {
        return jdbcMethod;
    }

	//ORACLE
	public void deleteTestRowOracle(String oracleDriver, String oracleConnection, String oracleUser, String oraclePassword, Properties Parameters, String tableName, String rowName, String DataCol) throws SQLException {
		String deleteRowSQL = "DELETE FROM " + tableName + "  WHERE " + rowName + " = " + DataCol1.toUpperCase();
		try {
			Class.forName(Param.getProperty("oracle_db_driver"));
		} catch (ClassNotFoundException e) {
			SuiteBase.Add_Log.error(e.getMessage() + ". " + e.getStackTrace().toString());
			SuiteBase.extentTest.log(LogStatus.ERROR, e.getMessage() + ". " + e.getStackTrace().toString());
		}
		
		try {
			dbConnection = DriverManager.getConnection(
					Param.getProperty("oracle_db_connection"),
					Param.getProperty("oracle_db_user"),
					Param.getProperty("oracle_db_password"));
			SuiteBase.Add_Log.info("Connection with Oracle database established");
			SuiteBase.extentTest.log(LogStatus.ERROR, "Connection with Oracle database established");
		} catch (SQLException e) {
			SuiteBase.Add_Log.error(e.getMessage() + ". " + e.getStackTrace().toString());
			SuiteBase.extentTest.log(LogStatus.ERROR, e.getMessage() + ". " + e.getStackTrace().toString());
			
		}
			
		statement = dbConnection.createStatement();
		try{
			statement.executeQuery(deleteRowSQL);
			SuiteBase.Add_Log.info("Clean up statement executed: " + deleteRowSQL);
			SuiteBase.extentTest.log(LogStatus.INFO, "Clean up statement executed: " + deleteRowSQL);
		}
		catch (SQLException e){
			SuiteBase.Add_Log.error(e.getMessage() + ". " + e.getStackTrace().toString());
		}
		if(statement != null) try { statement.close(); } catch(Exception e) {}
	    if(dbConnection != null) try { dbConnection.close(); } catch(Exception e) {}
	}
	
	///MSSQL
	public void deleteTestRowSQL(String mssqlDriver, String mssqlName, String mssqlUser, String mssqlPassword, Properties Parameters, String siteURL, String rowName, String DataCol){
	    if (Param == null){
	    	System.out.println("ok");
	    }else
	    {
	    	System.out.println("error");
	    }
	   String connectionUrl = 
	    		Param.getProperty("mssql_db_driver") + "; databaseName=" + 
	    		Param.getProperty("mssql_db_name") + "; user=" + 
	    		Param.getProperty("mssql_db_user") + "; password=" + 
	    		Param.getProperty("mssql_db_password");
	    String deleteRowOracle = "DELETE FROM " + tableName + "  WHERE " + rowName + " = " + DataCol1.toUpperCase();
	    try {
	      Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
	      dbConnection = DriverManager.getConnection(connectionUrl);
	      SuiteBase.Add_Log.info("Connection with MSSQL database established");
	      SuiteBase.extentTest.log(LogStatus.INFO, "Connection with MSSQL database established");
	      statement = dbConnection.createStatement();
	      statement.executeQuery(deleteRowOracle);
	      SuiteBase.Add_Log.info("Clean up statement executed: " + deleteRowOracle);
	      SuiteBase.extentTest.log(LogStatus.INFO, "Clean up statement executed: " + deleteRowOracle);
		  
	    }catch(Exception e){
	    	SuiteBase.Add_Log.error(e.getMessage() + ". " + e.getStackTrace().toString());
	    	SuiteBase.extentTest.log(LogStatus.ERROR, e.getMessage() + ". " + e.getStackTrace().toString());
	    }
	    if(statement != null) try { statement.close(); } catch(Exception e) {}
	    if(dbConnection != null) try { dbConnection.close(); } catch(Exception e) {}
	}
}	
