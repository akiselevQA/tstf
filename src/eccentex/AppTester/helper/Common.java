/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eccentex.AppTester.helper;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.util.Date;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.relevantcodes.extentreports.LogStatus;

import eccentex.TestSuite.Base.SuiteBase;

public final class Common {
    protected WebDriver driver;
    
    public final static int SLEEP_TIME_DEFAULT = 30;

    public Common(WebDriver driver) {
        this.driver = driver;
    }
    
    public void switchToDefaultContent() {
        SuiteBase.Add_Log.info("Switching to default content");
        SuiteBase.extentTest.log(LogStatus.INFO, "Switching to default content");
        driver.switchTo().defaultContent();
    }
    
    public void takeScreenshot(String fileName) {
        String fileId = String.valueOf(new Date().getTime()) + fileName;
        File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        Path path = FileSystems.getDefault().getPath("screenshots", fileId + ".png");
        try {
            FileUtils.copyFile(scrFile, new File(path.toString()));
            SuiteBase.Add_Log.info("Screenshot saved to: " + path.toString());
            SuiteBase.extentTest.log(LogStatus.INFO, "Screenshot saved to: " + path.toString());
        } catch (IOException ex) {
        	SuiteBase.Add_Log.info(ex.toString());
        	SuiteBase.extentTest.log(LogStatus.ERROR, ex.toString());
        }
    }

    public static void waitTime(int seconds) {
        try {
            Thread.sleep(seconds * 1000);
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
    }
    
    public WebElement waitCSS(String selector) throws Throwable {
        return waitCSS(selector, SLEEP_TIME_DEFAULT);
    }
    
    public WebElement waitCSS(String selector, int seconds) throws Throwable {
        return (new WebDriverWait(driver, seconds)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(selector)));
    }
    
    public WebElement waitId(String id) throws Throwable {
        return waitIdSeconds(id, SLEEP_TIME_DEFAULT);
    }
    
    public WebElement waitIdSeconds(String id, int seconds) throws Throwable {
        return (new WebDriverWait(driver, seconds)).until(ExpectedConditions.presenceOfElementLocated(By.id(id)));
    }

    public WebElement getParentElement(WebElement elArg) {
        return elArg.findElement(By.xpath(".."));
    }
}
