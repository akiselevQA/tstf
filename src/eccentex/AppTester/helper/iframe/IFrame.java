package eccentex.AppTester.helper.iframe;

import eccentex.AppTester.helper.ExtJs.Component;
import eccentex.TestSuite.Base.SuiteBase;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.relevantcodes.extentreports.LogStatus;

public class IFrame extends Component{
    public IFrame(WebElement elArg, WebDriver driver) {
        super(elArg, driver);
    }
    
	public void switchIntoFrame(String frameName){
		driver.switchTo().frame(frameName);
		SuiteBase.Add_Log.info("Switched to iframe : " + frameName);
		SuiteBase.extentTest.log(LogStatus.INFO, "Switched to iframe : " + frameName);
    }
    
    public void switchFromFrame(){
    	driver.switchTo().defaultContent();
    	SuiteBase.Add_Log.info("Swiched to default frame");
    	SuiteBase.extentTest.log(LogStatus.INFO, "Swiched to default frame");
    }
}