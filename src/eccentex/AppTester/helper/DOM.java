/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eccentex.AppTester.helper;

import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 *
 * @author ikoldaev
 */
public class DOM {
    protected WebDriver driver;
    
    //TODO: review: maybe make this class into DOMElement class 
    //TODO: that would bewrapper around WebElement with helpful methods to traverse and manipulate
    public DOM(WebDriver driver) {
        this.driver = driver;
    }
    
    public WebElement findParentByCSS(WebElement el, String selector) {
        return findParentByCSS(el, selector, 0);
    }
    
    public WebElement findParentByCSS(WebElement el, String selector, int maxLevel) {
        WebElement currentEl = el;
        WebElement prevEl = null;
        for (int currLevel = 0; currLevel < 999; currLevel++) {
            prevEl = currentEl;
            currentEl = currentEl.findElement(By.xpath(".."));
            if (currentEl == null || currentEl == prevEl) break;    // already on top
            if (elementMatchesCSS(currentEl, selector)) {
                return currentEl;
            }
            if (maxLevel > 0 && currLevel == maxLevel) break;   // maxLevel=0 means no limit
        }
        
        return null;
    }
    
    public boolean elementMatchesCSS(WebElement el, String selector) {
        WebElement parentEl = el.findElement(By.xpath(".."));
        List<WebElement> elementsList = parentEl.findElements(By.cssSelector(selector));
        return elementsList.contains(el);
    }
}
