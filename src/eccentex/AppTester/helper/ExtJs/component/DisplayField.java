/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eccentex.AppTester.helper.ExtJs.component;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.relevantcodes.extentreports.LogStatus;

import eccentex.TestSuite.Base.SuiteBase;

/**
 *
 * @author ikoldaev
 */
public class DisplayField extends FormField {
    public DisplayField(WebElement elArg, WebDriver driver) {
        super(elArg, driver);
    }

    @Override
    public String getValue() {
        SuiteBase.Add_Log.info("Checking display field value");
        SuiteBase.extentTest.log(LogStatus.INFO, "Checking display field value");
        return element.getText();
    }
}
