/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eccentex.AppTester.helper.ExtJs.component;

import eccentex.AppTester.helper.Common;
import eccentex.TestSuite.Base.SuiteBase;

import java.util.ArrayList;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.relevantcodes.extentreports.LogStatus;

/**
 *
 * @author Olga
 */
public class GridPanel extends Panel {
    public GridPanel(WebElement elArg, WebDriver driver) {
        super(elArg, driver);
    }

    public int getRowsCount() {
        SuiteBase.Add_Log.info("Counting number of rows");
        SuiteBase.extentTest.log(LogStatus.INFO, "Counting number of rows");
        List<WebElement> rows = element.findElements(By.cssSelector("div.x-grid3-row"));
        return rows.size();
    }

    public GridColumn findColumnByHeaderText(String headerTextArg) throws Throwable {
        SuiteBase.Add_Log.info("Searching for column by header: " + headerTextArg);
        SuiteBase.extentTest.log(LogStatus.INFO, "Searching for column by header: " + headerTextArg);
        //GridColumn col = new GridColumn();
        //col.setHeaderText(headerText);
        //(new WebDriverWait(driver, 30)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("tr.x-grid3-hd-row")));
        try {
            (new Common(driver)).waitCSS("tr.x-grid3-hd-row");
        } catch (Throwable t) {
            throw new Throwable("Seems like there is no header row for the grid.", t);
        }
        
        WebElement headerRow = element.findElement(By.cssSelector("tr.x-grid3-hd-row"));
        List<WebElement> headersList = headerRow.findElements(By.cssSelector("td.x-grid3-hd.x-grid3-cell"));
        GridColumn col = null;
        for (WebElement header : headersList) {
            if (header.getText().toLowerCase().contains(headerTextArg.toLowerCase())) {
                col = new GridColumn(header, driver);
                break;
            }
        }
        
        return col;
    }

    public GridRow findFirstRow() {
        SuiteBase.Add_Log.info("Searching for first grid row");
        SuiteBase.extentTest.log(LogStatus.INFO, "Searching for first grid row");
        (new WebDriverWait(driver, 30)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("div.x-grid3-row")));
        List<WebElement> rows = element.findElements(By.cssSelector("div.x-grid3-row"));
        return new GridRow(rows.get(0), driver);
    }

    public GridRow findRowByCellValue(GridColumn column, String value) throws Throwable {

        SuiteBase.Add_Log.info("Searching for row by cell value: " + value + " in column: " + column.getHeaderText());
        SuiteBase.extentTest.log(LogStatus.INFO, "Searching for row by cell value: " + value + " in column: " + column.getHeaderText());
        List<GridRow> matchingRowsList = findRowsByCellValue(column, value);
        if (matchingRowsList.size() < 1) {
            throw new Throwable(String.format("Cannot findRowByCellValue \"%s\"", value));
        }
        return matchingRowsList.get(0);
    }

    public List<GridRow> findRowsByCellValue(GridColumn column, String value) {

        SuiteBase.Add_Log.info("Searching for row by cell value: " + value + " in column: " + column.getHeaderText());
        SuiteBase.extentTest.log(LogStatus.INFO, "Searching for row by cell value: " + value + " in column: " + column.getHeaderText());
        (new WebDriverWait(driver, 30)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("div.x-grid3-row")));
        List<WebElement> rows = element.findElements(By.cssSelector("div.x-grid3-row"));
        List<GridRow> selectedRows = new ArrayList<GridRow>();
        for (int i = 0; i < column.getColumnCells().size(); i++) {
            if (value.equals(column.getColumnCells().get(i).getText())) {
                //System.out.println(rows.get(i).getText());
                selectedRows.add(new GridRow(rows.get(i), driver));
            }
        }
        return selectedRows;

    }
}
