/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eccentex.AppTester.helper.ExtJs.component;

import eccentex.AppTester.helper.ExtJs.Component;
import eccentex.TestSuite.Base.SuiteBase;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.relevantcodes.extentreports.LogStatus;

/**
 *
 * @author Olga
 * @author ikoldaev
 */
public class FormField extends Component{
    public FormField(WebElement elArg, WebDriver driver) {
        super(elArg, driver);
    }
    
    public void typeValue(String value){
        element.clear();
        element.sendKeys(value);
    }
    
    public String getValue(){
        SuiteBase.Add_Log.info("Checking form field value");
        SuiteBase.extentTest.log(LogStatus.INFO, "Checking form field value");
        return element.getAttribute("value");
    }
    
}
