/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eccentex.AppTester.helper.ExtJs.component;


import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 *
 * @author Olga
 * @author ikoldaev
 */
public class TextField extends FormField {
    
    public TextField(WebElement elArg, WebDriver driver) {
        super(elArg, driver);
    }
    
    public void clickSuggestedItemByText(String itemText){
        //item containing
        (new WebDriverWait(driver, 30)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(".suggestListBox")));
        List < WebElement > listItems = driver.findElements(By.cssSelector(".suggestListItem"));
        for (WebElement listItem: listItems){
            if (listItem.getText().contains(itemText)){
                listItem.click();
                break;
            }
        }
    }
}
