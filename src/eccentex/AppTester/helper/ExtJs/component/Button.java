package eccentex.AppTester.helper.ExtJs.component;


import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import eccentex.AppTester.helper.ExtJs.Component;

public class Button extends Component{

    public Button(WebElement elArg, WebDriver driver) {
        super(elArg, driver);
    }
    
    public boolean isSplitButton(){
        WebElement parent = element.findElement(By.xpath(".."));
        return parent.getAttribute("class").contains("split");
    }
    
    public void clickSplitButtonItemByText(String text){
        element.findElement(By.xpath("..")).click();
        WebElement menuList = (new WebDriverWait (driver, 30)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("ul.x-menu-list")));
        List <WebElement> menuItems = menuList.findElements(By.cssSelector(".x-menu-list-item "));
            for (WebElement menuItem: menuItems){
                System.out.println(menuItem.getText());
                if(menuItem.getText().contains(text)){
                    menuItem.click();
                    break;
                }
            }
            
            //TimeoutException
    }
    
}
