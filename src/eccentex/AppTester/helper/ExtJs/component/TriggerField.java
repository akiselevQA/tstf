/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eccentex.AppTester.helper.ExtJs.component;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.relevantcodes.extentreports.LogStatus;

import eccentex.TestSuite.Base.SuiteBase;

/**
 * @author Olga
 * @author ikoldaev
 */
public class TriggerField extends TextField {

    public TriggerField(WebElement elArg, WebDriver driver) {
        super(elArg, driver);
    }

    @Override
    public void click() {
        SuiteBase.Add_Log.info("Performing click on trigger");
        SuiteBase.extentTest.log(LogStatus.INFO, "Performing click on trigger");
        WebElement inputParent = element.findElement(By.xpath(".."));
        WebElement inputTrigger = inputParent.findElement(By.cssSelector(".x-form-trigger"));
        inputTrigger.click();
    }
}
