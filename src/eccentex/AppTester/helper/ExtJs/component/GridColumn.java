/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eccentex.AppTester.helper.ExtJs.component;


import eccentex.AppTester.helper.DOM;
import eccentex.AppTester.helper.ExtJs.Component;

import java.util.ArrayList;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 *
 * @author Olga
 * @author ikoldaev
 */
public class GridColumn extends Component{
    
    private String headerText;
    private List<WebElement> columnCells;
   
    public GridColumn(WebElement headerElArg, WebDriver driver) {
        super(headerElArg, driver);
        init(headerElArg);
    }
    
    // TODO: refactor
    public void setColumnCells(List<WebElement> columnCells){
        this.columnCells = columnCells;
    }
    
    public List<WebElement> getColumnCells(){
        return columnCells;
    }
    
    public String getHeaderText(){
        return headerText;
    }

    private void init(WebElement headerElArg) {
        headerText = headerElArg.getText();
        columnCells = new ArrayList<WebElement>();
        String headerClassFull = headerElArg.getAttribute("class");
        if (headerClassFull.contains("sort-desc")) {
            headerClassFull = headerClassFull.replace("sort-desc", "");
        }
        if (headerClassFull.contains("sort-asc")) {
            headerClassFull = headerClassFull.replace("sort-asc", "");
        }
        String headerClassNoSpace = headerClassFull.replaceAll("\\s+$", "");
        String pieceHeader = headerClassNoSpace.replace("x-grid3-hd ", "");
        String cssHeader = pieceHeader.replace(" ", ".");
        
        DOM domHelper = new DOM(driver);
        WebElement gridViewportEl = domHelper.findParentByCSS(headerElArg, ".x-grid3-viewport");
        
        
        List<WebElement> columnCellsAndHeader = gridViewportEl.findElements(By.cssSelector("td." + cssHeader));
        
        //loop to exclude header
        for (WebElement c : columnCellsAndHeader) {
            if (!c.equals(headerElArg)) {
                columnCells.add(c);
            }
        }
    }
    
}
