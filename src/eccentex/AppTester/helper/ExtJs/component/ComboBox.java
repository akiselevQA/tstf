/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eccentex.AppTester.helper.ExtJs.component;

//import static eccentex.AppTester.helper.ExtJs.component.Component.tpr;

import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.relevantcodes.extentreports.LogStatus;

import eccentex.TestSuite.Base.SuiteBase;

/**
 *
 * @author Olga
 */
public class ComboBox extends TriggerField {

    public ComboBox(WebElement elArg, WebDriver driver) {
        super(elArg, driver);
    }

    public void clickItemByText(String itemText) {
        //item containing
        SuiteBase.Add_Log.info("Clicking item " + itemText + " in combobox");
        SuiteBase.extentTest.log(LogStatus.INFO, "Clicking item " + itemText + " in combobox");
        (new WebDriverWait(driver, 30)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(".x-combo-list")));
        List< WebElement> listItems = driver.findElements(By.cssSelector(".x-combo-list-item"));
        for (WebElement listItem : listItems) {
            if (listItem.getText().contains(itemText)) {
                listItem.click();
                break;
            }
        }
    }

}
