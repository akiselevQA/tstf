/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eccentex.AppTester.helper.ExtJs.component;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.relevantcodes.extentreports.LogStatus;

import eccentex.AppTester.helper.Common;
import eccentex.AppTester.helper.ExtJs.ExtJs;
import eccentex.TestSuite.Base.SuiteBase;

public class TabPanel extends Panel {

    public TabPanel(WebElement elArg, WebDriver driver) {
        super(elArg, driver);
    }

    public Panel switchToTabByName(String name) throws Throwable {

        SuiteBase.Add_Log.info("Switching to tab panel: " + name);
        SuiteBase.extentTest.log(LogStatus.INFO, "Switching to tab panel: " + name);
        Panel selectedTabPanel = null;
        WebElement tabList = (new Common(driver)).waitCSS("ul.x-tab-strip");
        List<WebElement> tabItems = tabList.findElements(By.tagName("li"));
        for (WebElement tabItem : tabItems) {
            if (tabItem.getText().toLowerCase().contains(name.toLowerCase())) {
                tabItem.click();
                String prefix = element.getAttribute("id") + "__"; // TODO: review
                String divCode = tabItem.getAttribute("id").replace(prefix, "");
                selectedTabPanel = (Panel) (new ExtJs(driver)).findComponentById(divCode);
            }
        }
        
        if (selectedTabPanel == null) {
            throw new Throwable(String.format("Cannot switchToTabByName(\"%s\") tab not found", name));
        }
        
        return selectedTabPanel;
    }
}
