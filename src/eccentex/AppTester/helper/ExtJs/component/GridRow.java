package eccentex.AppTester.helper.ExtJs.component;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.relevantcodes.extentreports.LogStatus;

import eccentex.AppTester.helper.ExtJs.Component;
import eccentex.TestSuite.Base.SuiteBase;

public class GridRow extends Component {

    public GridRow(WebElement elArg, WebDriver driver) {
        super(elArg, driver);
    }

    public String getFieldValue(GridColumn field) {

        SuiteBase.Add_Log.info("Searching for cell value in column " + field);
        SuiteBase.extentTest.log(LogStatus.INFO, "Searching for cell value in column " + field);
        String value = "";
        List<WebElement> rowCells = element.findElements(By.cssSelector("td.x-grid3-col.x-grid3-cell"));
        String cellCss = field.getColumnCells().get(0).getAttribute("class");
        for (WebElement cell : rowCells) {
            if (cell.getAttribute("class").equals(cellCss)) {
                value = cell.getText();
            }
        }
        return value;

    }

    public void clickCell(GridColumn column) {

        SuiteBase.Add_Log.info("Clicking on cell in column " + column);
        SuiteBase.extentTest.log(LogStatus.INFO, "Clicking on cell in column " + column);
        List<WebElement> rowCells = element.findElements(By.cssSelector("td.x-grid3-col.x-grid3-cell"));
        String cellCss = column.getColumnCells().get(0).getAttribute("class");
        for (WebElement cell : rowCells) {
            if (cell.getAttribute("class").equals(cellCss)) {
                String linkText = cell.getText();
                cell.findElement(By.linkText(linkText)).click();
            }
        }

    }

    public void doubleClickCell(GridColumn column) {

        SuiteBase.Add_Log.info("Doube clicking on cell in column " + column);
        SuiteBase.extentTest.log(LogStatus.INFO, "Doube clicking on cell in column " + column);
        List<WebElement> rowCells = element.findElements(By.cssSelector("td.x-grid3-col.x-grid3-cell"));
        String cellCss = column.getColumnCells().get(0).getAttribute("class");
        for (WebElement cell : rowCells) {
            if (cell.getAttribute("class").equals(cellCss)) {
                Actions action = new Actions(driver);
                action.moveToElement(cell).doubleClick().build().perform();
            }
        }

    }

    public void clickActionByIconName(String iconName) {

        SuiteBase.Add_Log.info("Clicking on action by icon name: " + iconName);
        SuiteBase.extentTest.log(LogStatus.INFO, "Clicking on action by icon name: " + iconName);
        (new WebDriverWait(driver, 30)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("button.x-btn-text")));
        List<WebElement> actionButtons = element.findElements(By.cssSelector("button.x-btn-text"));
        for (WebElement b : actionButtons) {
            if (b.getAttribute("class").contains(iconName)) {
                b.click();
            }
        }
    }

}

//String s = null; 
/*
WebElement table = driver.findElement(By.id("tableview-1032-record-20"));
    List<WebElement> allrows = table.findElements(By.tagName("tr"));
    List<WebElement> allcols = table.findElements(By.tagName("td"));
    System.out.println("Number of rows in the table "+allrows.size());
    System.out.println("Number of columns in the table "+allcols.size());

    for(WebElement row: allrows){
        List<WebElement> Cells = row.findElements(By.tagName("td"));
        for(WebElement Cell:Cells){
        	System.out.println("cell value = " + Cell.getText());
            //s = s.concat(Cell.getText());   
            if (Cell.getText().contains("TEST2")){
            	System.out.println("TEST2 is present in the table");
            	Cell.click();
            	break;
            }
        }
        //if (s.isEmpty()){System.out.println("TEST2 is not available in the table");}
    }
    
*/
