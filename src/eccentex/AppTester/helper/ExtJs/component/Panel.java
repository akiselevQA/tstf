package eccentex.AppTester.helper.ExtJs.component;

import eccentex.AppTester.helper.ExtJs.Component;
import eccentex.TestSuite.Base.SuiteBase;

import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.relevantcodes.extentreports.LogStatus;

public class Panel extends Component {

    public Panel(WebElement elArg, WebDriver driver) {
        super(elArg, driver);
    }

    public Boolean isClosable() {

        Boolean isClosable = false;
        //check if tpr's panel
        WebElement parentElement = element.findElement(By.xpath(".."));
        if (parentElement.getAttribute("class").contains("x-tab-panel")) {
                //get id, get strip panel, if list item containing id has class closable return true
            //else return false
            SuiteBase.Add_Log.info("Checking whether panel is closable");
            SuiteBase.extentTest.log(LogStatus.INFO, "Checking whether panel is closable");
            String panelId = element.getAttribute("id");
            for (int i = 0; i < 3; i++) {
                parentElement = parentElement.findElement(By.xpath(".."));
            }
            WebElement tabStrip = parentElement.findElement(By.cssSelector("ul.x-tab-strip.x-tab-strip-top"));
            List<WebElement> tabs = tabStrip.findElements(By.tagName("li"));
            for (WebElement tab : tabs) {
                if (tab.getAttribute("class").contains("x-tab-strip-closable") && tab.getAttribute("id").contains(panelId)) {
                    isClosable = true;
                    break;
                    //tab.findElement(By.cssSelector("a.x-tab-strip-close")).click();
                }
            }
        } else if (element.getAttribute("class").contains("x-window")) {
            SuiteBase.Add_Log.info("Checking whether window is closable");
            SuiteBase.extentTest.log(LogStatus.INFO, "Checking whether window is closable");
            List<WebElement> closeButtons = element.findElements(By.cssSelector("div.x-tool.x-tool-close"));
            if (closeButtons.size() > 0) {
                isClosable = true;
            }
        }
        return isClosable;

    }

    public void close() {

        WebElement parentElement = element.findElement(By.xpath(".."));
        if (parentElement.getAttribute("class").contains("x-tab-panel")) {
                //get id, get strip panel, if list item containing id has class closable return true
            //else return false
            SuiteBase.Add_Log.info("Closing tab panel");
            SuiteBase.extentTest.log(LogStatus.INFO, "Closing tab panel");
            String panelId = element.getAttribute("id");
            for (int i = 0; i < 3; i++) {
                parentElement = parentElement.findElement(By.xpath(".."));
            }
            WebElement tabStrip = parentElement.findElement(By.cssSelector("ul.x-tab-strip.x-tab-strip-top"));
            List<WebElement> tabs = tabStrip.findElements(By.tagName("li"));
            for (WebElement tab : tabs) {
                if (tab.getAttribute("class").contains("x-tab-strip-closable") && tab.getAttribute("id").contains(panelId)) {
                    tab.findElement(By.cssSelector("a.x-tab-strip-close")).click();
                }
            }
        } else if (element.getAttribute("class").contains("x-window")) {
            SuiteBase.Add_Log.info("Checking window");
            SuiteBase.extentTest.log(LogStatus.INFO, "Checking window");
            List<WebElement> closeButtons = element.findElements(By.cssSelector("div.x-tool.x-tool-close"));
            if (closeButtons.size() > 0) {
                closeButtons.get(0).click();
            }
        }

    }
}
