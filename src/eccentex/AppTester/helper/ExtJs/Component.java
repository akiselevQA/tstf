package eccentex.AppTester.helper.ExtJs;

import eccentex.AppTester.helper.iframe.IFrame;
import eccentex.TestSuite.Base.SuiteBase;

import java.util.ArrayList;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.relevantcodes.extentreports.LogStatus;

public class Component {

    protected WebDriver driver;
    protected WebElement element;

    public Component(WebElement elArg, WebDriver driver) {
        this.driver = driver;
        this.element = elArg;
    }

    public WebElement getElement() {
        return element;
    }

    public List< Component> findChildrenByXType(String xtype, int level) {
        List< Component> cmpList = new ArrayList< Component>();
        switch (xtype) {
            case "panel": {
                List< WebElement> allSubChildren = element.findElements(By.cssSelector("div.x-panel-bwrap"));
                for (WebElement child : allSubChildren) {
                    child = child.findElement(By.xpath(".."));
                    int eachLevel = this.getRelativeLevelsDown(new Component(child, driver));
                    if (eachLevel == level) {
                        cmpList.add(new Component(child, driver));
                    }
                }
                break;
            }
            case "tabpanel": {
                List< WebElement> allSubChildren = element.findElements(By.cssSelector("div.x-tab-panel-bwrap"));
                for (WebElement child : allSubChildren) {
                    child = child.findElement(By.xpath(".."));
                    int eachLevel = this.getRelativeLevelsDown(new Component(child, driver));
                    if (eachLevel == level) {
                        cmpList.add(new Component(child, driver));
                    }
                }
                break;
            }
            case "grid": {
                List< WebElement> allChildren = element.findElements(By.cssSelector("div.x-grid-panel"));
                for (WebElement child : allChildren) {
                    int eachLevel = this.getRelativeLevelsDown(new Component(child, driver));
                    if (eachLevel == level) {
                        cmpList.add(new Component(child, driver));
                    }
                }
                break;
            }
            case "textfield": {
                List< WebElement> allChildren = element.findElements(By.cssSelector("div.x-form-item"));
                for (WebElement child : allChildren) {
                    int eachLevel = this.getRelativeLevelsDown(new Component(child, driver));
                    if (eachLevel == level) {
                        cmpList.add(new Component(child, driver));
                    }
                }
                break;
            }
        }

        return cmpList;
    }

    public List< Component> findDirectChildrenByXtype(String xtype) {
        return findChildrenByXType(xtype, 1);
    }

    public Component findChildByXType(String xtype, int level) {
        Component cmpChild = null;
        switch (xtype) {
            case "panel": {
                List< WebElement> allSubChildren = element.findElements(By.cssSelector("div.x-panel-bwrap"));
                for (WebElement child : allSubChildren) {
                    child = child.findElement(By.xpath(".."));
                    int eachLevel = this.getRelativeLevelsDown(new Component(child, driver));
                    if (eachLevel == level) {
                        cmpChild = new Component(child, driver);
                    }
                }
                break;
            }
            case "tabpanel": {
                List< WebElement> allSubChildren = element.findElements(By.cssSelector("div.x-tab-panel-bwrap"));
                for (WebElement child : allSubChildren) {
                    child = child.findElement(By.xpath(".."));
                    int eachLevel = this.getRelativeLevelsDown(new Component(child, driver));
                    if (eachLevel == level) {
                        cmpChild = new Component(child, driver);
                    }
                }
                break;
            }
            case "grid": {
                List< WebElement> allChildren = element.findElements(By.cssSelector("div.x-grid-panel"));
                for (WebElement child : allChildren) {
                    int eachLevel = this.getRelativeLevelsDown(new Component(child, driver));
                    if (eachLevel == level) {
                        cmpChild = new Component(child, driver);
                    }
                }
                break;
            }
            case "textfield": {
                List< WebElement> allChildren = element.findElements(By.cssSelector("div.x-form-item"));
                for (WebElement child : allChildren) {
                    int eachLevel = this.getRelativeLevelsDown(new Component(child, driver));
                    if (eachLevel == level) {
                        cmpChild = new Component(child, driver);
                    }
                }
                break;
            }
        }

        return cmpChild;
    }

    public Component findDirectChildByXtype(String xtype) {
        return findChildByXType(xtype, 1);
    }

    public Component getParentComponent() {
        WebElement parentPanel = element.findElement(By.xpath(".."));
        String parentClass = "";
        if (!"body".equals(parentPanel.getTagName())) {
            while (!"x-panel-bwrap".equals(parentClass)) {
                parentClass = parentPanel.getAttribute("class");
                parentPanel = parentPanel.findElement(By.xpath(".."));
            }
        } else {
            parentPanel = element;
        }
        return new Component(parentPanel, driver);
    }

    public int getRelativeLevelsUp(Component cmp) {
        Component originalCmp = new Component(element, driver);
        int level = 0;
        while (!cmp.getElement().equals(originalCmp.getElement())) {
            originalCmp = originalCmp.getParentComponent();
            level++;
        }
        return level;
        //need to catch in case order or parent child is backwards
    }

    public int getRelativeLevelsDown(Component cmp) {
        Component originalCmp = new Component(element, driver);
        return cmp.getRelativeLevelsUp(originalCmp);
        //need to catch in case goes backwards
    }
    
    /*UNUSED*/
    public String getComponentDetails() {
        String result = "Id: " + element.getAttribute("id") + "\nClass: " + element.getAttribute("class") + "\nTag:  " + element.getTagName();
        return result;
    }

    public void click() {
        SuiteBase.Add_Log.info("Performing click");
        SuiteBase.extentTest.log(LogStatus.INFO, "Performing click");
        element.click();
    }
    
    public void mouseOn(){
    	SuiteBase.Add_Log.info("Mouse on");
    	SuiteBase.extentTest.log(LogStatus.INFO, "Mouse on");
    	Actions builder = new Actions(driver);
    	//builder.moveToElement(element).build().perform();
    	builder.moveToElement(element);
    	builder.pause(1000);
    	builder.build().perform();
    }

    public void doubleClick() {
        SuiteBase.Add_Log.info("Performing double click");
        SuiteBase.extentTest.log(LogStatus.INFO, "Performing double click");
        Actions action = new Actions(driver);
        action.moveToElement(element).doubleClick().build().perform();

    }
    
    /*UNUSED*/
    public boolean hasIFrame() {
        SuiteBase.Add_Log.info("Checking for presence of iFrame");
        SuiteBase.extentTest.log(LogStatus.INFO, "Checking for presence of iFrame");
        List<WebElement> iFrames = element.findElements(By.tagName("iframe"));
        if (iFrames.size() > 0) {
            return true;
        } else {
            return false;
        }

    }

    public IFrame getIFrame() {
        SuiteBase.Add_Log.info("Retrieving iFrame");
        SuiteBase.extentTest.log(LogStatus.INFO, "Retrieving iFrame");
        (new WebDriverWait(driver, 30)).until(ExpectedConditions.presenceOfElementLocated(By.tagName("iframe")));
        List<WebElement> iFrames = element.findElements(By.tagName("iframe"));
        return new IFrame(iFrames.get(0), driver);

    }
    
    /*UNUSED*/
    public boolean isHidden() {
        SuiteBase.Add_Log.info("Checking whether element is hidden");
        SuiteBase.extentTest.log(LogStatus.INFO, "Checking whether element is hidden");
        if (element.getAttribute("class").contains("hide")) {
            return true;
        } else {
            return false;
        }

    }
}
