package eccentex.AppTester.helper.ExtJs;

import java.util.List;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.relevantcodes.extentreports.LogStatus;

import eccentex.AppTester.helper.ExtJs.component.Button;
import eccentex.AppTester.helper.ExtJs.component.Checkbox;
import eccentex.AppTester.helper.ExtJs.component.ComboBox;
import eccentex.AppTester.helper.ExtJs.component.DateField;
import eccentex.AppTester.helper.ExtJs.component.DisplayField;
import eccentex.AppTester.helper.ExtJs.component.FormField;
import eccentex.AppTester.helper.ExtJs.component.FormGroup;
import eccentex.AppTester.helper.ExtJs.component.GridColumn;
import eccentex.AppTester.helper.ExtJs.component.GridPanel;
import eccentex.AppTester.helper.ExtJs.component.GridRow;
import eccentex.AppTester.helper.ExtJs.component.Panel;
import eccentex.AppTester.helper.ExtJs.component.RadioButton;
import eccentex.AppTester.helper.ExtJs.component.TabPanel;
import eccentex.AppTester.helper.ExtJs.component.TextArea;
import eccentex.AppTester.helper.ExtJs.component.TextField;
import eccentex.AppTester.helper.ExtJs.component.Window;
import eccentex.TestSuite.Base.SuiteBase;

public class ExtJs {

    protected WebDriver driver;
    protected final static int MASK_WAIT_TIME_DEFAULT = 30;
    public static Properties Param = null;

    public ExtJs(WebDriver driver) {
        this.driver = driver;
    }

    public Panel findPanelByTitle(String title) {

        SuiteBase.Add_Log.info("Searching for panel with title: " + title);
        SuiteBase.extentTest.log(LogStatus.INFO, "Searching for panel with title: " + title);
        Panel p = null;
        List< WebElement> panels = driver.findElements(By.cssSelector("div.x-panel"));
        for (WebElement panelEl : panels) {
            List< WebElement> spans = panelEl.findElements(By.cssSelector("span.x-panel-header-text"));
            for (WebElement span : spans) {
                if (span.getText().equals(title)) {
                    p = new Panel(panelEl, driver);
                    break;
                }
            }
        }
        
        return p;
    }

    public Panel findPanelByTitle(Component parent, String title) {

        SuiteBase.Add_Log.info("Searching for panel with title: " + title + " and parent component: " + parent);
        SuiteBase.extentTest.log(LogStatus.INFO, "Searching for panel with title: " + title + " and parent component: " + parent);
        Panel p = null;
        List< WebElement> panels = parent.getElement().findElements(By.cssSelector("div.x-panel"));
        for (WebElement panel : panels) {
            List< WebElement> spans = panel.findElements(By.cssSelector("span.x-panel-header-text"));
            for (WebElement span : spans) {
                if (span.getText().equals(title)) {
                    p = new Panel(panel, driver);
                }
            }
        }
        return p;

    }

    public Window findWindowByTitle(String title) {

        SuiteBase.Add_Log.info("Searching for window with title: " + title);
        SuiteBase.extentTest.log(LogStatus.INFO, "Searching for window with title: " + title);
        Window p = null;
        (new WebDriverWait(driver, 30)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("div.x-window")));
        List< WebElement> panels = driver.findElements(By.cssSelector("div.x-window"));
        for (WebElement panel : panels) {
            List< WebElement> spans = panel.findElements(By.cssSelector("span.x-window-header-text"));
            for (WebElement span : spans) {
                if (span.getText().contains(title)) {
                    p = new Window(panel, driver);
                    return p;
                }
            }
        }
        return p;

    }

    public Component findComponentById(String id) {

        WebElement element = (new WebDriverWait(driver, 30)).until(ExpectedConditions.presenceOfElementLocated(By.id(id)));
        String elementClass = element.getAttribute("class");
        if (elementClass.contains("x-btn")) {
            SuiteBase.Add_Log.info("Searching for button with id: " + id);
            SuiteBase.extentTest.log(LogStatus.INFO, "Searching for button with id: " + id);
            return new Button(element, driver);
        } else if (elementClass.contains("x-form-group")) {
            SuiteBase.Add_Log.info("Searching for form group with id: " + id);
            SuiteBase.extentTest.log(LogStatus.INFO, "Searching for form group with id: " + id);
            return new FormGroup(element, driver);
        } else if (elementClass.contains("x-form-text x-form-field")) {
            //find all inputs with id
            WebElement parent = element.findElement(By.xpath(".."));
            String parentClass = parent.getAttribute("class");
            if (parentClass.contains("x-form-field-wrap x-form-field-trigger-wrap")) {
                //either a combobox or datefield
                List< WebElement> cboItems = parent.findElements(By.cssSelector("img.x-form-trigger.x-form-arrow-trigger"));
                List< WebElement> dateItems = parent.findElements(By.cssSelector("img.x-form-trigger.x-form-date-trigger"));
                if (cboItems.size() == 1) {
                    SuiteBase.Add_Log.info("Searching for combo box with id: " + id);
                    SuiteBase.extentTest.log(LogStatus.INFO, "Searching for combo box with id: " + id);
                    return new ComboBox(parent, driver);
                } else if (dateItems.size() == 1) {
                    SuiteBase.Add_Log.info("Searching for date field with id: " + id);
                    SuiteBase.extentTest.log(LogStatus.INFO, "Searching for date field with id: " + id);
                    return new DateField(parent, driver);
                } else {
                    return new FormField(parent, driver);
                }
            } else {
                SuiteBase.Add_Log.info("Searching for text field with id: " + id);
                SuiteBase.extentTest.log(LogStatus.INFO, "Searching for text field with id: " + id);
                return new TextField(element, driver);
            }
        } else if (elementClass.contains("x-form-textarea x-form-field")) {
            SuiteBase.Add_Log.info("Searching for text area with id: " + id);
            SuiteBase.extentTest.log(LogStatus.INFO, "Searching for text area with id: " + id);
            return new TextArea(element, driver);
        } else if (elementClass.contains("x-grid3-row")) {
            SuiteBase.Add_Log.info("Searching for grid row with id: " + id);
            SuiteBase.extentTest.log(LogStatus.INFO, "Searching for grid row with id: " + id);
            return new GridRow(element, driver);
        } else if (elementClass.contains("x-grid3-col")) {
            SuiteBase.Add_Log.info("Searching for grid column with id: " + id);
            SuiteBase.extentTest.log(LogStatus.INFO, "Searching for grid column with id: " + id);
            return new GridColumn(element, driver);
        } else if (elementClass.contains("x-panel")) {
            if (elementClass.contains("x-grid-panel")) {
                SuiteBase.Add_Log.info("Searching for grid panel with id: " + id);
                SuiteBase.extentTest.log(LogStatus.INFO, "Searching for grid panel with id: " + id);
                return new GridPanel(element, driver);
            } else {
                SuiteBase.Add_Log.info("Searching for panel with id: " + id);
                SuiteBase.extentTest.log(LogStatus.INFO, "Searching for panel with id: " + id);
                return new Panel(element, driver);
            }
        } else if (elementClass.contains("x-tab-panel")) {
            return new TabPanel(element, driver);
        } else {
            SuiteBase.Add_Log.info("Searching for component with id: " + id);
            SuiteBase.extentTest.log(LogStatus.INFO, "Searching for component with id: " + id);
            return new Component(element, driver);
        }

    }

    public Component findComponentById(Component parent, String id) {

        WebElement element = parent.getElement().findElement(By.id(id));
        String elementClass = element.getAttribute("class");
        if (elementClass.contains("x-btn")) {
            SuiteBase.Add_Log.info("Searching for button with id: " + id + " and parent: " + parent);
            SuiteBase.extentTest.log(LogStatus.INFO, "Searching for button with id: " + id + " and parent: " + parent);
            return new Button(element, driver);
        } else if (elementClass.contains("x-form-group")) {
            SuiteBase.Add_Log.info("Searching for form group with id: " + id + " and parent: " + parent);
            SuiteBase.extentTest.log(LogStatus.INFO, "Searching for form group with id: " + id + " and parent: " + parent);
            return new FormGroup(element, driver);
        } else if (elementClass.contains("x-form-text x-form-field")) {
            //find all inputs with id
            WebElement p = element.findElement(By.xpath(".."));
            String parentClass = p.getAttribute("class");
            if (parentClass.contains("x-form-field-wrap x-form-field-trigger-wrap")) {
                //either a combobox or datefield
                List< WebElement> cboItems = p.findElements(By.cssSelector("img.x-form-trigger.x-form-arrow-trigger"));
                List< WebElement> dateItems = p.findElements(By.cssSelector("img.x-form-trigger.x-form-date-trigger"));
                if (cboItems.size() == 1) {
                    SuiteBase.Add_Log.info("Searching for combobox with id: " + id + " and parent: " + parent);
                    SuiteBase.extentTest.log(LogStatus.INFO, "Searching for combobox with id: " + id + " and parent: " + parent);
                    return new ComboBox(p, driver);
                } else if (dateItems.size() == 1) {
                    SuiteBase.Add_Log.info("Searching for date field with id: " + id + " and parent: " + parent);
                    SuiteBase.extentTest.log(LogStatus.INFO, "Searching for date field with id: " + id + " and parent: " + parent);
                    return new DateField(p, driver);
                } else {
                    return new FormField(p, driver);
                }
            } else {
                SuiteBase.Add_Log.info("Searching for text field with id: " + id + " and parent: " + parent);
                SuiteBase.extentTest.log(LogStatus.INFO, "Searching for text field with id: " + id + " and parent: " + parent);
                return new TextField(element, driver);
            }
        } else if (elementClass.contains("x-form-textarea x-form-field")) {
            SuiteBase.Add_Log.info("Searching for text area with id: " + id + " and parent: " + parent);
            SuiteBase.extentTest.log(LogStatus.INFO, "Searching for text area with id: " + id + " and parent: " + parent);
            return new TextArea(element, driver);
        } else if (elementClass.contains("x-grid3-row")) {
            SuiteBase.Add_Log.info("Searching for grid row with id: " + id + " and parent: " + parent);
            SuiteBase.extentTest.log(LogStatus.INFO, "Searching for grid row with id: " + id + " and parent: " + parent);
            return new GridRow(element, driver);
        } else if (elementClass.contains("x-grid3-col")) {
            SuiteBase.Add_Log.info("Searching for grid column with id: " + id + " and parent: " + parent);
            SuiteBase.extentTest.log(LogStatus.INFO, "Searching for grid column with id: " + id + " and parent: " + parent);
            return new GridColumn(element, driver);
        } else if (elementClass.contains("x-panel")) {
            if (elementClass.contains("x-grid-panel")) {
                SuiteBase.Add_Log.info("Searching for grid panel with id: " + id + " and parent: " + parent);
                SuiteBase.extentTest.log(LogStatus.INFO, "Searching for grid panel with id: " + id + " and parent: " + parent);
                return new GridPanel(element, driver);
            } else {
                SuiteBase.Add_Log.info("Searching for panel with id: " + id + " and parent: " + parent);
                SuiteBase.extentTest.log(LogStatus.INFO, "Searching for panel with id: " + id + " and parent: " + parent);
                return new Panel(element, driver);
            }
        } else if (elementClass.contains("x-tab-panel")) {
            return new TabPanel(element, driver);
        } else {
            SuiteBase.Add_Log.info("Searching for component with id: " + id + " and parent: " + parent);
            SuiteBase.extentTest.log(LogStatus.INFO, "Searching for component with id: " + id + " and parent: " + parent);
            return new Component(element, driver);
        }

    }

    /*UNUSED*/
    public TextArea findTextAreaByLabel(String label) {

        SuiteBase.Add_Log.info("Searching for textarea with label: " + label);
        SuiteBase.extentTest.log(LogStatus.INFO, "Searching for textarea with label: " + label);
        List< WebElement> formFields = driver.findElements(By.cssSelector("div.x-form-item"));
        for (WebElement formField : formFields) {
            WebElement frmLabel = formField.findElement(By.tagName("label"));
            if (frmLabel.getText().contains(label)) {
                return new TextArea(formField.findElement(By.tagName("textarea")), driver);
            }
        }
        return null; //TODO: review

    }

    /*UNUSED*/
    public TextArea findTextAreaByLabel(Component parent, String label) {

        SuiteBase.Add_Log.info("Searching for textarea with label: " + label + " and parent: " + parent);
        SuiteBase.extentTest.log(LogStatus.INFO, "Searching for textarea with label: " + label + " and parent: " + parent);
        List< WebElement> formFields = parent.getElement().findElements(By.cssSelector("div.x-form-item"));
        for (WebElement formField : formFields) {
            WebElement frmLabel = formField.findElement(By.tagName("label"));
            if (frmLabel.getText().contains(label)) {
                return new TextArea(formField.findElement(By.tagName("textarea")), driver);
            }
        }
        return null; //TODO: review

    }

    /*UNUSED*/
    public TextField getTextFieldType(WebElement input) {
        WebElement inputParent = input.findElement(By.xpath(".."));
        List<WebElement> imgs = inputParent.findElements(By.cssSelector("img.x-form-trigger"));
        if (imgs.isEmpty()) {
            return new TextField(input, driver);
        } else if (imgs.get(0).getAttribute("class").contains("arrow")) {
            return new ComboBox(input, driver);
        } else if (imgs.get(0).getAttribute("class").contains("date")) {
            return new DateField(input, driver);
        } else {
            return null; // TODO: review
        }
    }
    
    /*UNUSED*/
    public FormField getFormFieldType(WebElement input) {
        if (input.getAttribute("type").equals("text")) {
            return getTextFieldType(input);
        } else if (input.getAttribute("type").equals("radio")) {
            return new RadioButton(input, driver);
        } else if (input.getAttribute("type").equals("checkbox")) {
            return new Checkbox(input, driver);
        } else {
            return new FormField(input, driver);
        }
    }

    /*UNUSED*/
    public FormField findInputByLabel(String label) {

        SuiteBase.Add_Log.info("Searching for input with label: " + label);
        SuiteBase.extentTest.log(LogStatus.INFO, "Searching for input with label: " + label);
        WebElement frmLabel = (new WebDriverWait(driver, 30)).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//label[contains(.,'" + label + "')]")));
        WebElement frmParent = frmLabel.findElement(By.xpath(".."));
        List<WebElement> inputs = frmParent.findElements(By.tagName("input"));
        if (inputs.size() == 1) {
            return getFormFieldType(inputs.get(0));
        } else {
            for (WebElement i : inputs) {
                if (!i.getAttribute("type").contains("hidden")) {
                    return getFormFieldType(i);
                }
            }
        }
        return null; //TODO: review
    }

    /*UNUSED*/
    public FormField findInputByLabel(Component parent, String label) {

        SuiteBase.Add_Log.info("Searching for input with label: " + label + " and parent: " + parent.getElement().getAttribute("id"));
        SuiteBase.extentTest.log(LogStatus.INFO, "Searching for input with label: " + label + " and parent: " + parent.getElement().getAttribute("id"));
        WebElement frmLabel = (new WebDriverWait(driver, 30)).until(ExpectedConditions.visibilityOf(parent.getElement().findElement(By.xpath("//label[contains(.,'" + label + "')]"))));
        WebElement frmParent = frmLabel.findElement(By.xpath(".."));
        List<WebElement> inputs = frmParent.findElements(By.tagName("input"));
        if (inputs.size() == 1) {
            return getFormFieldType(inputs.get(0));
        } else {
            for (WebElement i : inputs) {
                if (!i.getAttribute("type").contains("hidden")) {
                    return getFormFieldType(i);
                }
            }
            return null; // TODO: review
        }
    }
    
    /*UNUSED*/
    public DisplayField findDisplayFieldByLabel(String label) {

        SuiteBase.Add_Log.info("Searching for display field with label: " + label);
        SuiteBase.extentTest.log(LogStatus.INFO, "Searching for display field with label: " + label);
        List< WebElement> formFields = driver.findElements(By.cssSelector("div.x-form-item"));
        for (WebElement formField : formFields) {
            WebElement frmLabel = formField.findElement(By.tagName("label"));
            if (frmLabel.getText().contains(label)) {
                return new DisplayField(formField.findElement(By.cssSelector("div.x-form-display-field")), driver);
            }
        }
        return null; //TODO: review

    }

    /*UNUSED*/
    public DisplayField findDisplayFieldByLabel(Component parent, String label) {

        SuiteBase.Add_Log.info("Searching for display field with label: " + label + " in parent " + parent.getElement().getAttribute("id"));
        SuiteBase.extentTest.log(LogStatus.INFO, "Searching for display field with label: " + label + " in parent " + parent.getElement().getAttribute("id"));
        List< WebElement> formFields = parent.getElement().findElements(By.cssSelector("div.x-form-item"));
        for (WebElement formField : formFields) {
            WebElement frmLabel = formField.findElement(By.tagName("label"));
            if (frmLabel.getText().contains(label)) {
                return new DisplayField(formField.findElement(By.cssSelector("div.x-form-display-field")), driver);
            }
        }
        return null;  // TODO: review

    }

    public Button findButtonByIcon(String iconName) {
        SuiteBase.Add_Log.info("Searching for button by icon name: " + iconName);
        SuiteBase.extentTest.log(LogStatus.INFO, "Searching for button by icon name: " + iconName);
        iconName = iconName.toLowerCase();
        WebElement button = (new WebDriverWait(driver, 30)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("button.x-btn-text.icon-" + iconName)));
        return new Button(button, driver);
    }

    public Button findButtonByIcon(Component parent, String iconName) {
        SuiteBase.Add_Log.info("Searching for button by icon name: " + iconName + " and parent: " + parent.getElement().getAttribute("id"));
        SuiteBase.extentTest.log(LogStatus.INFO, "Searching for button by icon name: " + iconName + " and parent: " + parent.getElement().getAttribute("id"));
        iconName = iconName.toLowerCase();
        (new WebDriverWait(driver, 30)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("button.x-btn-text.icon-" + iconName)));
        WebElement button = parent.getElement().findElement(By.cssSelector("button.x-btn-text.icon-" + iconName));
        return new Button(button, driver);
    }

    public Button findButtonByText(String text) {
        SuiteBase.Add_Log.info("Searching for button with text: " + text);
        SuiteBase.extentTest.log(LogStatus.INFO, "Searching for button with text: " + text);
        try {
         WebElement button = driver.findElement(By.name(text)); 
         SuiteBase.Add_Log.info("Button " + text + " found");
         SuiteBase.extentTest.log(LogStatus.INFO, "Button " + text + " found");
       	 return new Button(button, driver);
       }
       catch (Exception e) {
		  	 SuiteBase.Add_Log.info("Button " + text + " not found");
		  	 SuiteBase.extentTest.log(LogStatus.INFO, "Button " + text + " not found");
		  	 return null;
       }
    }
    
    public Button findOKAlertButton() {
        SuiteBase.Add_Log.info("Searching for OK alert button");
        SuiteBase.extentTest.log(LogStatus.INFO, "Searching for OK alert button");
        try {
         WebElement button = (new WebDriverWait(driver, 30)).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//button[contains(@class, 'confirm')]")));
         SuiteBase.Add_Log.info("Button OK found");
         SuiteBase.extentTest.log(LogStatus.INFO, "Button OK found");
       	 return new Button(button, driver);
       }
       catch (Exception e) {
		  	 SuiteBase.Add_Log.info("Button OK not found");
		  	 SuiteBase.extentTest.log(LogStatus.INFO, "Button OK not found");
		  	 return null;
       }
    }
    
    public Button findElementByXPath(String type, String tag, String text) {
        SuiteBase.Add_Log.info("Searching for Element by x-path: " + text);
        SuiteBase.extentTest.log(LogStatus.INFO, "Searching for Element by x-path: " + text);
        try {
         WebElement button = (new WebDriverWait(driver, 30)).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//"+type+"[contains(@"+tag+",'" + text + "')]")));
         SuiteBase.Add_Log.info("Element " + text + " found");
         SuiteBase.extentTest.log(LogStatus.INFO, "Element " + text + " found");
       	 return new Button(button, driver);
       }
       catch (Exception e) {
		  	 SuiteBase.Add_Log.info("Element " + text + " not found");
		  	 SuiteBase.extentTest.log(LogStatus.INFO, "Element " + text + " not found");
		  	 return null;
       }
    }
    
    //Call this function to locate element by ID locator.
    public WebElement getElementByID(String id){
    	SuiteBase.Add_Log.info("Searching for element containing id: " + id);
    	SuiteBase.extentTest.log(LogStatus.INFO, "Searching for element containing id: " + id);
        try{
        	return driver.findElement(By.id(id));
        	//return driver.findElement(By.id(Object.getProperty(id)));
    	}catch(Throwable t){
    		SuiteBase.Add_Log.debug("Element not found by id: " + id);
    		SuiteBase.extentTest.log(LogStatus.ERROR, "Element not found by id: " + id);
    	return null;
    	}
    }
    
    public WebElement getGridRowByContent(String text){
    	SuiteBase.Add_Log.info("Searching for grid row with text: " + text);
    	try {
    		 WebElement webElement = driver.findElement(By.xpath("//td[contains(div,'" + text + "')]"));
    		 SuiteBase.Add_Log.info("There is row with text (upper case): " + text);
    		 SuiteBase.extentTest.log(LogStatus.INFO, "There is row with text (upper case): " + text);
     		 return webElement;
    	}
    	catch (Exception e) {
    		SuiteBase.Add_Log.info("There is no row with text (upper case): " + text);
    		SuiteBase.extentTest.log(LogStatus.INFO, "There is no row with text (upper case): " + text);
    		return null;
		}
    }
    	
    public Button findButtonByText(Component parent, String text) {
        (new WebDriverWait(driver, 30)).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//button[contains(.,'" + text + "')]")));
        SuiteBase.Add_Log.info("Searching for button with text: " + text + " and parent: " + parent.getElement().getAttribute("id"));
        SuiteBase.extentTest.log(LogStatus.INFO, "Searching for button with text: " + text + " and parent: " + parent.getElement().getAttribute("id"));
        WebElement button = parent.getElement().findElement(By.xpath("//button[contains(.,'" + text + "')]"));
        return new Button(button, driver);

    }
    
    /*UNUSED*/
    public Window findWinowByContainingText(String text) {
        SuiteBase.Add_Log.info("Searching for window containing text: " + text);
        List< WebElement> windows = driver.findElements(By.cssSelector("div.x-window"));
        for (WebElement window : windows) {
            if (window.getText().contains(text)) {
                return new Window(window, driver);
            }
        }
        return null; //TODO: review
    }
    
    public boolean verifyButtonsDisplayed(String[] buttons) {
        for (String b : buttons) {
            Button btn = findButtonByText(b);
            if (btn.getElement() == null) {
                return false;
            }
        }
        return true;
    }

    public boolean verifyButtonsDisplayed(Component parent, String[] buttons) {
        for (String b : buttons) {
            Button btn = findButtonByText(parent, b);
            if (btn.getElement() == null) {
                return false;
            }
        }
        return true;
    }

    public void loadingMaskWait() {
        loadingMaskWait(MASK_WAIT_TIME_DEFAULT);
    }
    
    public void loadingMaskWait(int seconds) {
        /*
        Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)
                .withTimeout(3, SECONDS)
                .pollingEvery(1, SECONDS)
                .ignoring(NoSuchElementException.class);
        */
        
        (new WebDriverWait(driver, seconds)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("div.x-mask-loading")));

        /*
        Boolean loadingMaskVisible = true;
        while (loadingMaskVisible) {
            try {
                WebElement loadingMask = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("div.x-mask-loading")));
            } catch (TimeoutException e) {
                loadingMaskVisible = false;
                break;
            }
        }
        */
    }

    /*UNUSED*/
    public boolean validationMessageCheck(Component component, String message) {
        SuiteBase.Add_Log.info("Checking validation message: " + message);
        SuiteBase.extentTest.log(LogStatus.INFO, "Checking validation message: " + message);
        try {
            if (component.getElement().getText().contains(message)) {
                return true;
            } else {
                return false;
            }
        } catch (NullPointerException ex) {
            return false;
        }
    }
    
	//check that error message displays on the page
	public boolean validateMessageExists(String emailErrorMessage) {
		boolean successStatus = false; 
		if (!(driver.getPageSource().contains(emailErrorMessage))){
			SuiteBase.Add_Log.debug("Error message '"+emailErrorMessage +"' expected");
			SuiteBase.extentTest.log(LogStatus.ERROR, "Error message '"+emailErrorMessage +"' expected");
			successStatus = false;
		}
		else
		{
			SuiteBase.Add_Log.info("Error message '"+emailErrorMessage +"' found");
			SuiteBase.extentTest.log(LogStatus.INFO, "Error message '"+emailErrorMessage +"' found");
			successStatus = true;
		}
		return successStatus;
	}
    
	/*UNUSED AND DUPLICATES
	//getElementByXPath function for static xpath
	public WebElement getElementByXPathStatic(String Key){
		try{
			//This block will find element using Key value from web page and return It.
			return driver.findElement(By.xpath(Object.getProperty(Key)));
		}catch(Throwable t){
			//If element not found on page then It will return null.
			Add_Log.debug("Object not found for key --"+Key);
			return null;
		}
	}
	
	//getElementByXPath function for dynamic xpath
	public WebElement getElementByXPathDynamic(String Key1, int val, String key2){
		try{
			//This block will find element using values of Key1, val and key2 from web page and return It.
			return driver.findElement(By.xpath(Object.getProperty(Key1)+val+Object.getProperty(key2)));
		}catch(Throwable t){
			//If element not found on page then It will return null.
			Add_Log.debug("Object not found for custom xpath");
			return null;
		}
	}
	
	//Call this function to locate element by Name Locator.
	public WebElement getElementByName(String Key){
		try{
			return driver.findElement(By.name(Object.getProperty(Key)));
		}catch(Throwable t){
			Add_Log.debug("Object not found for key --"+Key);
			return null;
		}
	}
	
	//Call this function to locate element by cssSelector Locator.
	public WebElement getElementByCSS(String Key){
		try{
			return driver.findElement(By.cssSelector(Object.getProperty(Key)));
		}catch(Throwable t){
			Add_Log.debug("Object not found for key --"+Key);
			return null;
		}
	}
	
	//Call this function to locate element by ClassName Locator.
	public WebElement getElementByClass(String Key){
		try{
			return driver.findElement(By.className(Object.getProperty(Key)));
		}catch(Throwable t){
			Add_Log.debug("Object not found for key --"+Key);
			return null;
		}
	}
	
	//Call this function to locate element by tagName Locator.
	public WebElement getElementByTagName(String Key){
		try{
			return driver.findElement(By.tagName(Object.getProperty(Key)));
		}catch(Throwable t){
			Add_Log.debug("Object not found for key --"+Key);
			return null;
		}
	}
	
	//Call this function to locate element by link text Locator.
	public WebElement getElementByFullLinkText(String Key){
		try{
			return driver.findElement(By.linkText(Object.getProperty(Key)));
		}catch(Throwable t){
			Add_Log.debug("Object not found for key --"+Key);
			return null;
		}
	}
	
	//Call this function to locate element by partial link text Locator.
	public WebElement getElementByLinkText(String Key){
		try{
			return driver.findElement(By.partialLinkText(Object.getProperty(Key)));
		}catch(Throwable t){
			Add_Log.debug("Object not found for key --"+Key);
			return null;
		}
	}
	*/
	
	//Compare title of the current page with expected title
		public boolean compareTitle(boolean testfail, String expectedResult) {
		//Compare title of the page
				String actualTitle = driver.getTitle();
				if(!(actualTitle.equals(expectedResult))){
					//If expected and actual results not match, Set flag testFail=true.
					testfail=true;	
					//If result Is fail then test failure will be captured Inside s_assert object reference.
					//This soft assertion will not stop your test execution.
					SuiteBase.Add_Log.debug("Actual Result Title "+actualTitle+" And Expected Title Value "+expectedResult+" Not Match");
					SuiteBase.extentTest.log(LogStatus.ERROR, "Actual Result Title "+actualTitle+" And Expected Title Value "+expectedResult+" Not Match");
				}
		return testfail;
		}
		
		//check that field format is correct
		public boolean verifyTextFieldPattern(boolean testFail, String DataColX, String pattern, String errorMessage) {
			java.util.regex.Pattern p = java.util.regex.Pattern.compile(pattern);
	        java.util.regex.Matcher m = p.matcher(DataColX);
	        if (!m.matches()){
	        	if (!(driver.getPageSource().contains(errorMessage))){
	        		SuiteBase.Add_Log.debug("Error text "+errorMessage +" Expected for Value '"+DataColX + "'");
	        		SuiteBase.extentTest.log(LogStatus.ERROR, "Error text "+errorMessage +" Expected for Value '"+DataColX + "'");
	    			testFail = true;
	        	}
	        }
	        return testFail;
		}
		
		public void sleep(){
			try {
				Thread.sleep(Integer.parseInt(Param.getProperty("waitTime")));
				SuiteBase.Add_Log.info("Sleep for :" + Param.getProperty("waitTime"));
				SuiteBase.extentTest.log(LogStatus.INFO, "Sleep for :" + Param.getProperty("waitTime"));
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	
		//assertThat(summary.getAttribute("value"), is(SUMMARY));

		public void findRowTDHorizontal(String text){
			(new WebDriverWait(driver, 3)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(".x-grid")));
			WebElement grid = driver.findElement(By.cssSelector(".x-grid"));
			List<WebElement> tds = grid.findElements(By.tagName("td"));
			for (int i = 0; i < tds.size(); i++) {
			        WebElement cell = tds.get(i);
			        String curTest = cell.getText();
			        if (curTest.contains(text)) {
			            int index = 0; //click on the first row
	
			            List<WebElement> buttons = grid.findElements(By.cssSelector("span.x-btn-icon-el-default-small"));
			            buttons.get(index).click();
			            break;
			        }
			    }
			SuiteBase.Add_Log.info("Row actions menu opened");
			SuiteBase.extentTest.log(LogStatus.INFO, "Row actions menu opened");

		}

		//Find record in the grid
		public void findRowTRVertical(String text){
			(new WebDriverWait(driver, 3)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(".x-grid")));
			//TestUtils.waitToBePresent(driver, By.cssSelector(".x-grid"), 30);
		    WebElement grid = driver.findElement(By.cssSelector(".x-grid"));
		    //  WebElement sppOrder_table = driver.findElement(By.xpath("(//*[contains(@id, 'messages-grid-')])[1]"));
		    List<WebElement> numOfRows = grid.findElements(By.tagName("tr"));
		     for (WebElement row : numOfRows) {
		         if (row.findElement(By.cssSelector("td:nth-child(4)")).getText().equals(text))
		             row.findElement(By.cssSelector("span.x-btn-icon-el-default-small")).click();
		    }
		    SuiteBase.extentTest.log(LogStatus.INFO, "Row actions menu opened");
		    //model.findOptionByTextInButtonMenu("Disable").click();
		    //model.findOptionByTextInButtonMenu("Delete").click();
		}

		public void validateFieldIsNotEditable(String string) {
			SuiteBase.Add_Log.info("Validate field is read only: " + string);
			SuiteBase.extentTest.log(LogStatus.INFO, "Validate field is read only: " + string);
			WebElement some_element = getElementByID(string);
		    String readonly = some_element.getAttribute("disabled");
		    Assert.assertEquals(readonly, "true");
		    SuiteBase.Add_Log.info("Field " + string + "is read only");
		    SuiteBase.extentTest.log(LogStatus.INFO, "Field " + string + "is read only");
		}
		
}
