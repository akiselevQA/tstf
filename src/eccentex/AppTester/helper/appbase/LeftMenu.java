/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eccentex.AppTester.helper.appbase;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.relevantcodes.extentreports.LogStatus;

import eccentex.TestSuite.Base.SuiteBase;

public class LeftMenu {
    protected WebDriver driver;

    public LeftMenu(WebDriver driver) {
        this.driver = driver;
    }
    
    public int clickItemByName(String name) {
        // TODO: Review: switch to default context maybe? Can break rare use-cases.
        SuiteBase.Add_Log.info("Try to clik Left Menu item: " + name);
        SuiteBase.extentTest.log(LogStatus.INFO, "Try to clik Left Menu item: " + name);
        
        WebElement elementFound = null;
        elementFound = (new WebDriverWait(driver, 30)).until(ExpectedConditions.presenceOfElementLocated(By.linkText(name)));
        if (elementFound != null){
        	(new WebDriverWait(driver, 30)).until(ExpectedConditions.presenceOfElementLocated(By.linkText(name))).click();
        	SuiteBase.Add_Log.info("Click on menu item : " + name);
        	SuiteBase.extentTest.log(LogStatus.INFO, "Click on menu item : " + name);
			return 1;
		}
		else{
			SuiteBase.Add_Log.info("Click on menu item : " + name + "failed");
			SuiteBase.extentTest.log(LogStatus.INFO, "Click on menu item : " + name + "failed");
			return 0;
		}

    }
    
    public int validateItemByNameExists(String name) {
        SuiteBase.Add_Log.info("Searching for Left Menu item: " + name);
        SuiteBase.extentTest.log(LogStatus.INFO, "Searching for Left Menu item: " + name);
        WebElement elementFound = null;
        elementFound = (new WebDriverWait(driver, 30)).until(ExpectedConditions.presenceOfElementLocated(By.linkText(name)));
		if (elementFound != null){
			SuiteBase.Add_Log.info("Searching for " + name + " : Successfully found menu item");
			SuiteBase.extentTest.log(LogStatus.INFO, "Searching for " + name + " : Successfully found menu item");
			return 1;
		}
		else{
			SuiteBase.Add_Log.info("Searching for " + name + " : Executed with errors");
			SuiteBase.extentTest.log(LogStatus.INFO, "Searching for " + name + " : Executed with errors");
			return 0;
		}
    }
}
