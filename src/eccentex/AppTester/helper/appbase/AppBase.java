/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eccentex.AppTester.helper.appbase;

import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.relevantcodes.extentreports.LogStatus;

import eccentex.AppTester.helper.Helpers;
import eccentex.TestSuite.Base.SuiteBase;

public class AppBase {
	public static Properties Param = null;
	public static Properties Object = null;
	protected WebDriver driver;
	
    public AppBase(WebDriver driver) {
    	this.driver = driver;
    }
    
    
    public void loginAndOpenSolution(String siteURL, String username, String password, String menuItemName, Properties Param, Helpers helpers, WebDriver driver)
    		throws Throwable {
    		
    		//LOGIN
            helpers.getAppbase().login(siteURL, username, password);
            //helpers.getCommon().waitId("pnCenter_IFrame");
            driver.switchTo().defaultContent();
            //driver.switchTo().frame("pnCenter_IFrame");
            
            //VALIDATE MENU ITEM EXISTS
            //return helpers.getLeftmenu().validateItemByNameExists(menuItemName);
    }

    
    
    public boolean login(String url, String username, String password) {
    	SuiteBase.Add_Log.info("Loading web page at " + url);
		driver.get(url);
        try {
            WebElement element = (new WebDriverWait(driver, 4)).until(ExpectedConditions.presenceOfElementLocated(By.id("username")));
            SuiteBase.Add_Log.info("Found username field");
            SuiteBase.extentTest.log(LogStatus.INFO, "Found username field");
            element.clear();
            SuiteBase.Add_Log.info("Adding username: " + username);
            SuiteBase.extentTest.log(LogStatus.INFO, "Adding username: " + username);
            element.sendKeys(username);
            element = driver.findElement(By.name("password"));
            SuiteBase.Add_Log.info("Adding password: " + password);
            SuiteBase.extentTest.log(LogStatus.INFO, "Adding password: " + password);
            element.sendKeys(password);
            element.submit();
            SuiteBase.Add_Log.info("Clicked login button");
            SuiteBase.extentTest.log(LogStatus.INFO, "Clicked login button");
            return true;
        } catch (TimeoutException ex) {
        	SuiteBase.Add_Log.warn("Already logged in or browser is not working");
        	SuiteBase.extentTest.log(LogStatus.ERROR, "Already logged in or browser is not working");
        	return true;
        	/*logout();
            WebElement element = (new WebDriverWait(driver, 4)).until(ExpectedConditions.presenceOfElementLocated(By.id("username")));
            SuiteBase.Add_Log.info("Found username field");
            element.clear();
            SuiteBase.Add_Log.info("Adding username: " + username);
            element.sendKeys(username);
            element = driver.findElement(By.name("password"));
            SuiteBase.Add_Log.info("Adding password: " + password);
            element.sendKeys(password);
            element.submit();
            SuiteBase.Add_Log.info("Clicked login button");
            return true;
            */
        }
    }

    /* OUT OF DATE
	public void logout() {
        SuiteBase.Add_Log.info("Logging out");
        driver.switchTo().defaultContent();
        List<WebElement> topElements = driver.findElements(By.cssSelector("div#TopNavigationControl1 .x-btn-text"));
        for (WebElement item : topElements) {
            if (item.getText().equals("Logout")) {
                item.click();
            }
        }
        List<WebElement> LogoutDialogButtons = driver.findElements(By.cssSelector(".x-btn-text"));
        for (WebElement item : LogoutDialogButtons) {
            if (item.getText().equals("Yes")) {
                item.click();
                break;
            }
        }
    } */
}
