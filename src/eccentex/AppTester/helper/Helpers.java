/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eccentex.AppTester.helper;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import eccentex.AppTester.JDBC.JDBCMethod;
import eccentex.AppTester.helper.ExtJs.ExtJs;
import eccentex.AppTester.helper.appbase.AppBase;
import eccentex.AppTester.helper.appbase.LeftMenu;
import eccentex.AppTester.helper.file.FileExtJs;
import eccentex.AppTester.helper.iframe.IFrame;

public class Helpers {
    protected WebDriver driver;

    protected Common common;
    protected DOM dom;
    protected ExtJs extjs;
    protected AppBase appbase;
    protected LeftMenu leftmenu;
	protected IFrame iframe;
	protected WebElement element;
	protected JDBCMethod jdbcMethod;
	protected FileExtJs fileHelper;
    
    public Helpers(WebDriver driver) {
        this.driver=driver;
    }

    public Common getCommon() {
        if (common == null) {
            common = new Common(driver);
        }
        return common;
    }

    public DOM getDom() {
        if (dom == null) {
        	new DOM(driver);
        }
        return dom;
    }

    public ExtJs getExtjs() {
        if (extjs == null) {
            extjs = new ExtJs(driver);
        }
        return extjs;
    }
    
    public AppBase getAppbase() {
        if (appbase == null) {
            appbase = new AppBase(driver);
        }
        return appbase;
    }

    public LeftMenu getLeftmenu() {
        if (leftmenu == null) {
            leftmenu = new LeftMenu(driver);
        }
        return leftmenu;
    }
    
    public IFrame getIFrame() {
        //if (iframe == null) {
            iframe = new IFrame(element, driver);
        //}
        return iframe;
    }
    
    public FileExtJs getFileHelper() {
        if (fileHelper == null) {
        	fileHelper = new FileExtJs(fileHelper);
        }
        return fileHelper;
    }
    
    public JDBCMethod getJDBCMethod() {
        if (jdbcMethod == null) {
        	jdbcMethod = new JDBCMethod(jdbcMethod);
        }
        return jdbcMethod;
    } 
}
