package eccentex.AppTester.driver;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

import com.relevantcodes.extentreports.LogStatus;

import eccentex.AppTester.helper.Common;
import eccentex.AppTester.utility.Read_XLS;
import eccentex.AppTester.utility.SuiteUtility;
import eccentex.TestSuite.Base.SuiteBase;

public class SeleniumWebDriver {
	protected WebDriver driver;
	public static WebDriver existingchromeBrowser;
	public static WebDriver existingmozillaBrowser;
	public static WebDriver existingIEBrowser;
	
	public WebDriver loadDriver(String testBrowser) throws IOException{
		//Check If any previous webdriver browser Instance Is exist then run new test In that existing webdriver browser Instance.
   	SuiteBase.Add_Log.info("Test browser: " + testBrowser);
   	SuiteBase.extentTest.log(LogStatus.INFO, "Test browser: " + testBrowser);
   	if(testBrowser.equalsIgnoreCase("Mozilla") && existingmozillaBrowser!=null){
			driver = existingmozillaBrowser;
			return driver;
		}else if(testBrowser.equalsIgnoreCase("Chrome") && existingchromeBrowser!=null){
			driver = existingchromeBrowser;
			return driver;
		}else if(testBrowser.equalsIgnoreCase("IE") && existingIEBrowser!=null){
			driver = existingIEBrowser;
			return driver;
		}else if(testBrowser.equalsIgnoreCase("Mozilla")&& existingIEBrowser==null){
			//To Load Firefox driver Instance. 
			//https://github.com/SeleniumHQ/selenium/issues/3633
			
			  DesiredCapabilities caps = DesiredCapabilities.firefox();
    			caps.setBrowserName("firefox");
    			caps.setPlatform(org.openqa.selenium.Platform.WIN10);
			 
			driver = new FirefoxDriver();
			existingmozillaBrowser=driver;
			SuiteBase.Add_Log.info("Firefox Driver Instance loaded successfully.");
			SuiteBase.extentTest.log(LogStatus.INFO, "Firefox Driver Instance loaded successfully.");
		}else if(testBrowser.equalsIgnoreCase("Chrome")&& existingIEBrowser==null){
			//To Load Chrome driver Instance.
			//System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"//BrowserDrivers//chromedriver.exe");
			System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
			try{
				driver = new ChromeDriver();
			}
			catch (Exception e) {
				System.out.println(e);
			}
			existingchromeBrowser=driver;
			SuiteBase.Add_Log.info("Chrome Driver Instance loaded successfully.");
			SuiteBase.extentTest.log(LogStatus.INFO, "Chrome Driver Instance loaded successfully.");
		}else if(testBrowser.equalsIgnoreCase("IE")&& existingIEBrowser==null){
			//To Load IE driver Instance.
			System.setProperty("webdriver.ie.driver", "IEDriverServer.exe");
			try{
				DesiredCapabilities caps = DesiredCapabilities.internetExplorer();
				caps.setCapability(InternetExplorerDriver.IE_ENSURE_CLEAN_SESSION, true);
				caps.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS,
						true);
				driver = new InternetExplorerDriver(caps);
			}
			catch (Exception e) {
				System.out.println(e);
			}
			existingIEBrowser=driver;
			SuiteBase.Add_Log.info("IE Driver Instance loaded successfully.");
			SuiteBase.extentTest.log(LogStatus.INFO, "IE Driver Instance loaded successfully.");
		}			
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		Common.waitTime(2);
		return driver;
	}
	
	//add logs while closing the browser
	 	public void closeWebDriver(String testCaseName, boolean testCasePass, Read_XLS filePath, String sheetName){
	 		if(testCasePass){
	 			SuiteBase.Add_Log.info(testCaseName+" : Reporting test case as PASS In excel.");
	 			SuiteBase.extentTest.log(LogStatus.INFO, testCaseName+" : Reporting test case as PASS In excel.");
	 			SuiteUtility.WriteResultUtility(filePath, sheetName, "Pass/Fail/Skip", testCaseName, "PASS");
	 		}
	 		else{
	 			SuiteBase.Add_Log.error(testCaseName+" : Reporting test case as FAIL In excel.");
	 			SuiteBase.extentTest.log(LogStatus.FAIL, testCaseName+" : Reporting test case as FAIL In excel.");
	 			SuiteUtility.WriteResultUtility(filePath, sheetName, "Pass/Fail/Skip", testCaseName, "FAIL");			
	 		}
	 		driver.close();
	 		//null browser Instance when close.
	 		existingchromeBrowser=null;
	 		existingmozillaBrowser=null;
	 		existingIEBrowser=null;
	 	}
	 	
	 	public WebDriver getDriver() {
	        return driver;
	    }

}
