package eccentex.AppTester.utility;

import org.apache.log4j.Logger;

public class Logging {
	public static Logger Add_Log = null;
	
	public static Logger getLogger() {
        if (Add_Log == null) {
        	Add_Log = Logger.getLogger("rootLogger");
        	Add_Log.info("Log4j initialized");
        }
        return Add_Log;
    }
}
