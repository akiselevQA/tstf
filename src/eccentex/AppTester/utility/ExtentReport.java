package eccentex.AppTester.utility;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import eccentex.TestSuite.Base.SuiteBase;

public class ExtentReport {
	public static ExtentReports extentReports;
	protected static ExtentTest extentTest;
	
	public static ExtentReports getExtentReport() {
		if (extentReports == null){
        	//get current date
    		SimpleDateFormat sdfDate = new SimpleDateFormat("MM/dd/yy HH:mm a");
    		Date now = new Date();
    		String currentDateTime = sdfDate.format(now);
 			
    		extentReports = new ExtentReports(System.getProperty("user.dir") +"/ExtentReport/index.html", true);
			System.out.println(System.getProperty("user.dir") +"/ExtentReport/index.html");
			extentReports
		        .addSystemInfo("Environment", "Eccentex Components")
		        .addSystemInfo("Run date", currentDateTime);
			}
		return extentReports;
	}

	public static ExtentTest getExtentTest(String testName) {
		if (extentTest == null){
			extentTest = extentReports.startTest(testName);
			SuiteBase.extentTest.log(LogStatus.INFO, testName + " configureation started");
		}
		return extentTest;
	}
}
