//Find More Tutorials On WebDriver at -> http://software-testing-tutorials-automation.blogspot.com
package eccentex.AppTester.utility;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

import eccentex.TestSuite.Base.SuiteBase;


public class ScreenshotUtility implements ITestListener{	
	//This method will execute before starting of Test suite.
	public void onStart(ITestContext tr) {	
		
	}

	//This method will execute, Once the Test suite is finished.
	public void onFinish(ITestContext tr) {
		
	}

	//This method will execute only when the test is pass.
	public void onTestSuccess(ITestResult tr) {
		//If screenShotOnPass = yes, call captureScreenShot.
		if(SuiteBase.Param.getProperty("screenShotOnPass").equalsIgnoreCase("yes"))
		{
			captureScreenShot(tr,"screenshots/Success");
		}
	}

	//This method will execute only on the event of fail test.
	public void onTestFailure(ITestResult tr) {		
		//If screenShotOnFail = yes, call captureScreenShot.
		if(SuiteBase.Param.getProperty("screenShotOnFail").equalsIgnoreCase("yes"))
		{
			captureScreenShot(tr,"screenshots/Failures");
		}
	}

	// This method will execute before the main test start (@Test)
	public void onTestStart(ITestResult tr) {
		
	}

	// This method will execute only if any of the main test(@Test) get skipped
	public void onTestSkipped(ITestResult tr) {		
	}
	
	public void onTestFailedButWithinSuccessPercentage(ITestResult tr) {
	}
	
	//Function to capture screenshot.
	public static String captureScreenShot(ITestResult result, String destDir){	
		String destFile ="";
		String passFailMethod = result.getMethod().getRealClass().getSimpleName() + "." + result.getMethod().getMethodName();
		//To capture screenshot.
		File scrFile = ((TakesScreenshot)SuiteBase.driver).getScreenshotAs(OutputType.FILE);
    	DateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy__hh_mm_ssaa");
    	    	
    	//Set file name with combination of test class name + date time.
    	destFile = passFailMethod+" - "+dateFormat.format(new Date()) + ".png";
    	
        try {
        	//Store file at destination folder location
     	   if (destDir == null){
     		   FileUtils.copyFile(scrFile, new File("ExtentReport/"+destFile));
     	   }
     	   else{
     		//To create folder to store screenshots
     	    new File(destDir).mkdirs();
     		FileUtils.copyFile(scrFile, new File(destDir + "/" + destFile));
     	   }
        }
        catch (IOException e) {
             e.printStackTrace();
        }
        return destFile;
   }

}